﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, ICharacter
{
    //GameData
    private GameObject gameData;

    //Public fields
    public int PlayerID;
    public CharacterAsset charAsset;
    public PlayerArea playerArea;
    public SpellEffect wandPowerEffect;
    public bool usedWandPowerThisTurn = false;

    //References to logic that belongs to player
    public Deck deck;
    public Hand hand;
    public Table table;

    //Static array that stores both players
    public static Player[] Players;

    //Used for coin spell but we can TODO: extend this to other uses later
    private int bonusManaThisTurn = 0;
    //Sloth damage is taken when there are no more cards to draw. Incremented at every draw
    private int slothDamage = 1;
    //Track the number of cards played this turn by player. Reset on turn end to 0
    public int cardsPlayedThisTurn = 0;

    //Part of ICharacter interface
    public int ID
    {
        get
        {
            return PlayerID;
        }
    }

    public Player otherPlayer
    {
        get
        {
            if (Players[0] == this)
            {
                return Players[1];
            }
            else
            {
                return Players[0];
            }
        }
    }

    private int manaThisTurn;
    public int ManaThisTurn
    {
        get
        {
            return manaThisTurn;
        }
        set
        {
            if(value < 0)
            {
                manaThisTurn = 0;
            }
            //TODO change this as player should not have direct access to visual element in game
            else if(value > playerArea.manaCount.crystals.Length){
                manaThisTurn = playerArea.manaCount.crystals.Length;
            }
            else
            {
                manaThisTurn = value;
            }
            new UpdateManaCrystalsCommand(this, manaThisTurn, manaLeft).AddToQ();
        }
    }

    private int manaLeft;
    public int ManaLeft
    {
        get
        {
            return manaLeft;
        }
        set
        {
            //TODO change this as player should not have direct access to visual element in game
            if (value == 0)
            {
                manaLeft = 0;
            }
            else if(value > playerArea.manaCount.crystals.Length)
            {
                manaLeft = playerArea.manaCount.crystals.Length;
            }
            else
            {
                manaLeft = value;
            }
            new UpdateManaCrystalsCommand(this, ManaThisTurn, manaLeft).AddToQ();
            if(TurnManager.Instance.WhoseTurn == this)
            {
                HighlightPlayableCards();
            }
        }
    }

    private int health;
    public int Health
    {
        get
        {
            return health;
        }
        set
        {
            if (value > charAsset.MaxHealth)
            {
                health = charAsset.MaxHealth;
            }
            else
            {
                health = value;
            }
            if (value <= 0)
            {
                Die();
            }
        }
    }


    //CODE FOR SERVANTS TO KNOW WHEN TO CAUSE EFFECTS
    public delegate void VoidWithNoArguments();
    public event VoidWithNoArguments EndTurnEvent;
    public event VoidWithNoArguments StartTurnEvent;
    public event VoidWithNoArguments SpellPlayedEvent;
    public event VoidWithNoArguments ServantPlayedEvent;


    void Awake()
    {
        gameData = GameObject.Find("GameData");

        Players = GameObject.FindObjectsOfType<Player>();
        PlayerID = IDFactory.GetUniqueID();
        if (gameObject.name.Contains("BOTTOM"))
        {
            charAsset = gameData.GetComponent<GameData>().playerAsset;
        }
        else if(gameObject.name.Contains("TOP"))
        {
            charAsset = gameData.GetComponent<GameData>().enemyAsset;
        }
    }

    public virtual void OnTurnStart()
    {
        if(StartTurnEvent != null)
        {
            StartTurnEvent.Invoke();
        }
        usedWandPowerThisTurn = false;
        ManaThisTurn++;
        ManaLeft = ManaThisTurn;
        foreach(ServantLogic servantLogic in table.ServantsOnTable)
        {
            servantLogic.OnTurnStart();
        }
        playerArea.wandPowerButton.WasUsed = false;
    }

    public void OnTurnEnd()
    {
        cardsPlayedThisTurn = 0;
        if (EndTurnEvent != null)
        {
            EndTurnEvent.Invoke();
        }
        ManaThisTurn = ManaThisTurn - bonusManaThisTurn;
        bonusManaThisTurn = 0;
        foreach(ServantLogic servantLogic in table.ServantsOnTable)
        {
            servantLogic.OnTurnEnd();
        }
        GetComponent<TurnMaker>().StopAllCoroutines();
    }

    public void GetBonusMana(int ammount)
    {
        bonusManaThisTurn = bonusManaThisTurn + ammount;
        ManaThisTurn = ManaThisTurn + ammount;
        ManaLeft = ManaLeft + ammount;
    }

    public void DrawACard(bool fast = false)
    {
        if(deck.cards.Count > 0)
        {
            if(hand.CardsInHand.Count < playerArea.hand.slots.objects.Length)
            {
                //Save index to place card into
                int indexToPlaceACard = hand.CardsInHand.Count;
                //Add card to hand
                CardLogic newCard = new CardLogic(deck.cards[0]);
                newCard.owner = this;
                hand.CardsInHand.Add(newCard);
                //Remove card from deck
                deck.cards.RemoveAt(0);
                //Create command
                new DrawACardCommand(this, indexToPlaceACard, hand.CardsInHand[indexToPlaceACard], fast, fromDeck: true).AddToQ();
            }
        }
        else
        {
            //Take sloth damage
            new DealDamageCommand(ID, slothDamage, Health - slothDamage).AddToQ();
            Health -= slothDamage;
            slothDamage++;
        }
    }

    public void GetCardNotFromDeck(CardAsset cardToAdd)
    {
        if (hand.CardsInHand.Count < playerArea.hand.slots.objects.Length)
        {
            CardLogic card = new CardLogic(cardToAdd);
            card.owner = this;
            hand.CardsInHand.Add(card);

            new DrawACardCommand(this, hand.CardsInHand.Count - 1, hand.CardsInHand[hand.CardsInHand.Count - 1], fast: false, fromDeck: false).AddToQ();
        }
    }

    public void PlayASpellFromHand(int SpellCardUniqueID, int TargetUniqueID)
    {
        if (TargetUniqueID < 0)
        {
            PlayASpellFromHand(CardLogic.CardsCreatedThisGame[SpellCardUniqueID], null);
        }
        else if (TargetUniqueID == ID)
        {
            PlayASpellFromHand(CardLogic.CardsCreatedThisGame[SpellCardUniqueID], this);
        }
        else if (TargetUniqueID == otherPlayer.ID)
        {
            PlayASpellFromHand(CardLogic.CardsCreatedThisGame[SpellCardUniqueID], this.otherPlayer);
        }
        else
        {
            //Target is a servant
            PlayASpellFromHand(CardLogic.CardsCreatedThisGame[SpellCardUniqueID], ServantLogic.ServantsCreatedThisGame[TargetUniqueID]);
        }
    }

    public void PlayASpellFromHand(CardLogic playedCard, ICharacter target)
    {
        cardsPlayedThisTurn++;

        if(SpellPlayedEvent != null)
        {
            SpellPlayedEvent.Invoke();
        }
        ManaLeft = ManaLeft - playedCard.CurrentManaCost;
        //Cause effect
        if(playedCard.effect != null)
        {
            playedCard.effect.ActivateEffect(playedCard.ca.SpecialSpellAmmount, target);
        }
        else
        {
            Debug.Log("No effect found on card");
        }
        //Move to PlayACardSpot
        new PlayASpellCommand(playedCard, this).AddToQ();
        //Remove card from hand
        hand.CardsInHand.Remove(playedCard);
    }

    public void PlayAServantFromHand(int uniqueID, int tablePos, int targetUniqueID)
    {
        if(targetUniqueID < 0)
        {
            PlayAServantFromHand(CardLogic.CardsCreatedThisGame[uniqueID], tablePos);
        }
        else if(targetUniqueID == ID)
        {
            PlayAServantFromHand(CardLogic.CardsCreatedThisGame[uniqueID], tablePos, this);
        }
        else if(targetUniqueID == otherPlayer.ID)
        {
            PlayAServantFromHand(CardLogic.CardsCreatedThisGame[uniqueID], tablePos, this.otherPlayer);
        }
        else
        {
            //Target is servant
            PlayAServantFromHand(CardLogic.CardsCreatedThisGame[uniqueID], tablePos, ServantLogic.ServantsCreatedThisGame[targetUniqueID]);
        }
    }

    public void PlayAServantFromHand(CardLogic playedCard, int tablePos, ICharacter target = null)
    {
        cardsPlayedThisTurn++;

        //Events that trigget when servants are played/summoned (not divination)
        if (ServantPlayedEvent != null)
        {
            ServantPlayedEvent.Invoke();
        }
        ManaLeft = ManaLeft - playedCard.CurrentManaCost;
        //Create new servant and add to table
        ServantLogic newServant = new ServantLogic(this, playedCard.ca);
        table.ServantsOnTable.Insert(tablePos, newServant);
        //Move card to PlayACardSpot
        new PlayAServantCommand(playedCard, tablePos, this, newServant.uniqueServantID).AddToQ();
        //Cause Divination effect if any
        if(newServant.servantEffect != null)
        {
            newServant.servantEffect.WhenServantIsPlayed(newServant.cardAsset.SpecialServantAmmount, target);
        }

        //Remove card from hand
        hand.CardsInHand.Remove(playedCard);
        HighlightPlayableCards();
    }

    public void SummonServantNotFromHand(CardLogic playedCard, int tablePos)
    {
        //Events that trigget when servants are played/summoned (not divination)
        if (ServantPlayedEvent != null)
        {
            ServantPlayedEvent.Invoke();
        }
        //Create new servant and add to table
        ServantLogic newServant = new ServantLogic(this, playedCard.ca);
        table.ServantsOnTable.Insert(tablePos, newServant);
        //Move card to PlayACardSpot
        new PlayAServantCommand(playedCard, tablePos, this, newServant.uniqueServantID).AddToQ();
    }

    public void Die()
    {
        playerArea.controlsON = false;
        otherPlayer.playerArea.controlsON = false;
        TurnManager.Instance.StopTheTimer();
        CardAssetsList.CardAssets.Clear();
        OnGameComplete(this);
        new GameOverCommand(this).AddToQ();
    }

    public void OnGameComplete(Player looser)
    {
        if (looser.gameObject.name.Contains("BOTTOM"))
        {
            new ShowMessageCommand("You Lose", 2f).AddToQ();
        }
        else
        {
            new ShowMessageCommand("You Win!", 2f).AddToQ();
        }
    }
    public void UseWandPower()
    {
        //TODO: Change hardcoded 2
        ManaLeft = ManaLeft - 2;
        usedWandPowerThisTurn = true;
        wandPowerEffect.ActivateEffect();
    }

    //Highlights wand power, playable cards in hand and attackable servants on table if they can be used
    //TODO: Remove highlighting from player script and make it linked with commands to visual
    public void HighlightPlayableCards(bool removeAllHighlights = false)
    {
        foreach(CardLogic cardLogic in hand.CardsInHand)
        {
            GameObject g = IDHolder.GetGameObjectWIthID(cardLogic.UniqueCardID);
            if(g != null)
            {
                g.GetComponent<OneCardManager>().CanBePlayed = (cardLogic.CurrentManaCost <= ManaLeft) && cardLogic.CanBePlayed && !removeAllHighlights;
            }
        }

        foreach (ServantLogic servantLogic in table.ServantsOnTable)
        {
            GameObject g = IDHolder.GetGameObjectWIthID(servantLogic.uniqueServantID);
            if (g != null)
            {
                g.GetComponent<OneServantManager>().CanAttack = (servantLogic.AttacksLeftThisTurn > 0) && !removeAllHighlights;
            }
        }
        //TODO manaLeft not sure yet what to be so set it > 1
        playerArea.wandPowerButton.Highlighted = (!usedWandPowerThisTurn) && (manaLeft > 1) && !removeAllHighlights;
    }

    public void LoadCharacterInfoFromAsset()
    {
        Health = charAsset.MaxHealth;
        //Change the visuals for illustration, wand power
        playerArea.playerIllustration.characterAsset = charAsset;
        playerArea.playerIllustration.ReadCharacterFromAsset();
        //TODO: Wand power script attachment
        if(charAsset.WandPowerName != null && charAsset.WandPowerName != "")
        {
            wandPowerEffect = System.Activator.CreateInstance(System.Type.GetType(charAsset.WandPowerName)) as SpellEffect;
        }
        else
        {
            Debug.LogWarning("Check wand power names for character " + charAsset.ClassName);
        }
    }

    //TEST Comment this OUT!!
    public void TransmitInfoAboutPlayerToVisual()
    {
        playerArea.playerIllustration.gameObject.AddComponent<IDHolder>().UniqueID = PlayerID;

        if(GetComponent<TurnMaker>() is AITurnMaker)
        {
            playerArea.AllowedToControlThisPlayer = false;
        }
        else
        {
            playerArea.AllowedToControlThisPlayer = true;
        }
    }

    //TESTING ONLY
    /*
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            DrawACard();
        }
    }
    */
}
