﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[System.Serializable]
public class CardLogic : IIdentifiable
{
    public Player owner;
    public int UniqueCardID;
    public CardAsset ca;

    //TEST Comment bottom line out?
    public GameObject visualRepresentation;

    // Script of type SpellEffect that will be attached to this card when it's created
    public SpellEffect effect;

    //Static dictionary for managing IDs
    public static Dictionary<int, CardLogic> CardsCreatedThisGame = new Dictionary<int, CardLogic>();


    public int ID
    {
        get
        {
            return UniqueCardID;
        }
    }

    public int CurrentManaCost
    {
        get;
        set;
    }

    public bool CanBePlayed
    {
        get
        {
            bool ownersTurn = (TurnManager.Instance.WhoseTurn == owner);
            bool manaAvailable = (CurrentManaCost <= owner.ManaLeft);
            bool fieldNotFull = true;
            if(ca.MaxHealth > 0)
            {
                fieldNotFull = (owner.table.ServantsOnTable.Count < 6);
            }
            return ownersTurn && manaAvailable && fieldNotFull;
        }
    }

    public CardLogic(CardAsset ca)
    {
        this.ca = ca;
        UniqueCardID = IDFactory.GetUniqueID();
        ResetManaCost();
        // Create instance of SpellEffect with a name from CardAsset and attach it to
        if(ca.SpellScriptName != null && ca.SpellScriptName != "")
        {
            effect = System.Activator.CreateInstance(System.Type.GetType(ca.SpellScriptName)) as SpellEffect;
        }
        CardsCreatedThisGame.Add(UniqueCardID, this);
    }

    public void ResetManaCost()
    {
        CurrentManaCost = ca.ManaCost;
    }

}
