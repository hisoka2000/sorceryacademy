﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class RopeTimer : MonoBehaviour, IEventSystemHandler
{
    public float timeForOneTurn;
    private float timeTillZero;
    private bool counting = false;

    [SerializeField]
    public UnityEvent TimerExpired = new UnityEvent();

    public void StartTimer()
    {
        timeTillZero = timeForOneTurn;
        counting = true;
        new RopeTimerCommand(timeForOneTurn).AddToQ(); //TODO: Fix this if addToQ takes to long to initiate rope timer inGame
    }

    public void StopTimer()
    {
        counting = false;
        new RopeTimerCommand(); //Overloaded ropetimercommand constructor that stops timer
    }

    void Update()
    {
        if (counting)
        {
            timeTillZero = timeTillZero - Time.deltaTime;

            if (timeTillZero <= 0)
            {
                counting = false;
                TimerExpired.Invoke();
            }
        }
    }

}