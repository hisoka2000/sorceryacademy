﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour
{
    //Shuffles list of cards

    public List<CardAsset> cards = new List<CardAsset>();
    private GameObject gameData;
    private Player Player;
    CharacterAsset charAsset;

    //Shuffles Cards in BattleData
    public void Awake()
    {
        gameData = GameObject.Find("GameData");
        LoadDeck();
        cards.Shuffle();
    }

    void LoadDeck()
    {
        if (gameObject.name.Contains("BOTTOM")){
            charAsset = gameData.GetComponent<GameData>().playerAsset;
        }
        else
        {
            charAsset = gameData.GetComponent<GameData>().enemyAsset;
        }

        if (charAsset.ClassName == "Sylvanas")
        {
            cards = gameData.GetComponent<GameData>().NecromancerDeck;
        }
        else if (charAsset.ClassName == "Hani")
        {
            cards = gameData.GetComponent<GameData>().PyromancerDeck;
        }
        else if (charAsset.ClassName == "Jeina")
        {
            cards = gameData.GetComponent<GameData>().OracleDeck;
        }
        else if (charAsset.ClassName == "V")
        {
            cards = gameData.GetComponent<GameData>().SaintDeck;
        }
    }
}
