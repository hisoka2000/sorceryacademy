﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTurnMaker : TurnMaker
{
    public override void OnTurnStart()
    {
        base.OnTurnStart();
        //Display "Your Turn" message
        new ShowMessageCommand("Your Turn", 2f).AddToQ();
        player.DrawACard();
    }
}
