﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AITurnMaker : TurnMaker
{
    public override void OnTurnStart()
    {
        base.OnTurnStart();
        new ShowMessageCommand("Enemy's Turn!", 2.0f).AddToQ();
        player.DrawACard();
        StartCoroutine(MakeAITurn());
    }

    IEnumerator MakeAITurn()
    {
        bool AttackFirst = false;
        if(Random.Range(0, 2) == 0)
        {
            AttackFirst = true;
        }

        while (MakeOneAIMove(AttackFirst))
        {
            yield return null;
        }

        InsertDelay(1f);

        TurnManager.Instance.EndTurn();
    }

    bool MakeOneAIMove(bool attackFirst)
    {
        if (Command.isCardDrawPending())
        {
            return true;
        }
        else if (attackFirst)
        {
            return AttackWithAServant() || PlayACardFromHand() || UseWandPower();
        }
        else
        {
            return PlayACardFromHand() || AttackWithAServant() || UseWandPower();
        }
    }

    bool PlayACardFromHand()
    {
        foreach (CardLogic c in player.hand.CardsInHand)
        {
            if (c.CanBePlayed)
            {
                if (c.ca.MaxHealth == 0)
                {
                    // code to play a spell from hand
                    if (c.ca.targets == TargettingOption.NoTarget)
                    {
                        //If spell has no targetting just play it
                        player.PlayASpellFromHand(c, null);
                        InsertDelay(1.5f);
                        //Debug.Log("Card: " + c.ca.name + " can be played");
                        return true;
                    }
                    else if (c.ca.isPositiveEffect)
                    {
                        //Else, if spell has targetting, if it is a positive effect target allies
                        if(c.ca.targets == TargettingOption.AllCharacters || c.ca.targets == TargettingOption.YourCharacters)
                        {
                            //Target ally characters
                            int randomAllyCharacter = Random.Range(0, player.table.ServantsOnTable.Count + 1);
                            if (randomAllyCharacter == player.table.ServantsOnTable.Count || player.table.ServantsOnTable.Count == 0)
                            {
                                //target is player
                                player.PlayASpellFromHand(c, player);
                                InsertDelay(1.5f);
                                return true;
                            }
                            else
                            {
                                //target is servant
                                ServantLogic randomServant = player.table.ServantsOnTable[randomAllyCharacter];
                                player.PlayASpellFromHand(c, randomServant);
                                InsertDelay(1.5f);
                                return true;
                            }
                        }
                        else
                        {
                            if(player.table.ServantsOnTable.Count != 0)
                            {
                                //Target ally servants
                                int randomAllyServant = Random.Range(0, player.table.ServantsOnTable.Count);
                                ServantLogic randomServant = player.table.ServantsOnTable[randomAllyServant];
                                player.PlayASpellFromHand(c, randomServant);
                                InsertDelay(1.5f);
                                return true;
                            }
                        }
                    }
                    else if (!c.ca.isPositiveEffect)
                    {
                        //Else, if spell has targetting, if it is a negative effect target enemies
                        if (c.ca.targets == TargettingOption.AllCharacters || c.ca.targets == TargettingOption.EnemyCharacters)
                        {
                            //Target enemy characters
                            int randomEnemyCharacter = Random.Range(0, player.otherPlayer.table.ServantsOnTable.Count + 1);
                            if (randomEnemyCharacter == player.otherPlayer.table.ServantsOnTable.Count || player.otherPlayer.table.ServantsOnTable.Count == 0)
                            {
                                //target is enemy player
                                player.PlayASpellFromHand(c, player.otherPlayer);
                                InsertDelay(1.5f);
                                return true;
                            }
                            else
                            {
                                //target is enemy servant
                                ServantLogic randomServant = player.otherPlayer.table.ServantsOnTable[randomEnemyCharacter];
                                player.PlayASpellFromHand(c, randomServant);
                                InsertDelay(1.5f);
                                return true;
                            }
                        }
                        else
                        {
                            if (player.otherPlayer.table.ServantsOnTable.Count != 0)
                            {
                                //Target enemy servants
                                int randomEnemyServant = Random.Range(0, player.otherPlayer.table.ServantsOnTable.Count);
                                ServantLogic randomServant = player.otherPlayer.table.ServantsOnTable[randomEnemyServant];
                                player.PlayASpellFromHand(c, randomServant);
                                InsertDelay(1.5f);
                                return true;
                            }
                        }
                    }
                }
                else if(player.table.ServantsOnTable.Count < 6)
                {
                    // code to play a servant from hand
                    if (c.ca.targets == TargettingOption.NoTarget)
                    {
                        //If servant has no targetting just play it
                        player.PlayAServantFromHand(c, 0);
                        InsertDelay(1.5f);
                        //Debug.Log("Card: " + c.ca.name + " can be played");
                        return true;
                    }
                    else if (c.ca.isPositiveEffect)
                    {
                        //Else, if servant has targetting, if it is a positive effect target allies
                        if (c.ca.targets == TargettingOption.AllCharacters || c.ca.targets == TargettingOption.YourCharacters)
                        {
                            //Target ally characters
                            int randomAllyCharacter = Random.Range(0, player.table.ServantsOnTable.Count + 1);
                            if (randomAllyCharacter == player.table.ServantsOnTable.Count || player.table.ServantsOnTable.Count == 0)
                            {
                                //target is player
                                player.PlayAServantFromHand(c, 0, player);
                                InsertDelay(1.5f);
                                return true;
                            }
                            else
                            {
                                //target is servant
                                ServantLogic randomServant = player.table.ServantsOnTable[randomAllyCharacter];
                                player.PlayAServantFromHand(c, 0, randomServant);
                                InsertDelay(1.5f);
                                return true;
                            }
                        }
                        else
                        {
                            if (player.table.ServantsOnTable.Count != 0)
                            {
                                //Target ally servants
                                int randomAllyServant = Random.Range(0, player.table.ServantsOnTable.Count);
                                ServantLogic randomServant = player.table.ServantsOnTable[randomAllyServant];
                                player.PlayAServantFromHand(c, 0, randomServant);
                                InsertDelay(1.5f);
                                return true;
                            }
                        }
                    }
                    else if (!c.ca.isPositiveEffect)
                    {
                        //Else, if servant has targetting, if it is a negative effect target enemies
                        if (c.ca.targets == TargettingOption.AllCharacters || c.ca.targets == TargettingOption.EnemyCharacters)
                        {
                            //Target enemy characters
                            int randomEnemyCharacter = Random.Range(0, player.otherPlayer.table.ServantsOnTable.Count + 1);
                            if (randomEnemyCharacter == player.otherPlayer.table.ServantsOnTable.Count || player.otherPlayer.table.ServantsOnTable.Count == 0)
                            {
                                //target is enemy player
                                player.PlayAServantFromHand(c, 0, player.otherPlayer);
                                InsertDelay(1.5f);
                                return true;
                            }
                            else
                            {
                                //target is enemy servant
                                ServantLogic randomServant = player.otherPlayer.table.ServantsOnTable[randomEnemyCharacter];
                                player.PlayAServantFromHand(c, 0, randomServant);
                                InsertDelay(1.5f);
                                return true;
                            }
                        }
                        else
                        {
                            if (player.otherPlayer.table.ServantsOnTable.Count != 0)
                            {
                                //Target enemy servants
                                int randomEnemyServant = Random.Range(0, player.otherPlayer.table.ServantsOnTable.Count);
                                ServantLogic randomServant = player.otherPlayer.table.ServantsOnTable[randomEnemyServant];
                                player.PlayAServantFromHand(c, 0, randomServant);
                                InsertDelay(1.5f);
                                return true;
                            }
                        }
                    }
                }

            }
            //Debug.Log("Card: " + c.ca.name + " can NOT be played");
        }
        return false;
    }

    bool UseWandPower()
    {
        if (player.ManaLeft >= 2 && !player.usedWandPowerThisTurn)
        {
            //TODO: Targetting for wand power
            player.UseWandPower();
            InsertDelay(1.5f);
            //Debug.Log("AI used hero power");
            return true;
        }
        return false;
    }

    bool AttackWithAServant()
    {
        foreach (ServantLogic sl in player.table.ServantsOnTable)
        {
            if (sl.AttacksLeftThisTurn > 0)
            {
                // attack a random target with a creature
                if (player.otherPlayer.table.ServantsOnTable.Count > 0)
                {
                    //If there is a charm servant in opponents table attack it
                    bool attackedCharmServant = false;
                    foreach(ServantLogic EnemyServant in player.otherPlayer.table.ServantsOnTable)
                    {
                        if (EnemyServant.cardAsset.Charm)
                        {
                            sl.AttackServant(EnemyServant);
                            attackedCharmServant = true;
                            break;
                        }
                    }
                    if (!attackedCharmServant)
                    {
                        //if there is no charm servant, attack a random servant instead
                        int index = Random.Range(0, player.otherPlayer.table.ServantsOnTable.Count);
                        ServantLogic targetCreature = player.otherPlayer.table.ServantsOnTable[index];
                        sl.AttackServant(targetCreature);
                    }
                }
                else
                    sl.GoFace();

                InsertDelay(1f);
                //Debug.Log("AI attacked with creature");
                return true;
            }
        }
        return false;
    }

    void InsertDelay(float delay)
    {
        new DelayCommand(delay).AddToQ();
    }
}
