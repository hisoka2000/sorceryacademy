﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TurnMaker : MonoBehaviour
{
    protected Player player;

    void Awake()
    {
        player = GetComponent<Player>();
    }

    public virtual void OnTurnStart()
    {
        //add one mana crystal
        player.OnTurnStart();
    }
}
