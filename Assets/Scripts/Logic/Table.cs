﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Table : MonoBehaviour
{
    public List<ServantLogic> ServantsOnTable = new List<ServantLogic>();

    public void PlaceServantAt(int index, ServantLogic servant)
    {
        ServantsOnTable.Insert(index, servant);
    }
}
