﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ServantLogic : ICharacter
{
    public Player owner;
    public CardAsset cardAsset;
    public ServantEffect servantEffect;
    public int uniqueServantID;
    //STATIC ServantsCreatedThisGame for managing IDs
    public static Dictionary<int, ServantLogic> ServantsCreatedThisGame = new Dictionary<int, ServantLogic>();
    public static List<ServantLogic> ServantsDiedThisGame = new List<ServantLogic>();


    //Property from ICharacter interface
    public int ID
    {
        get
        {
            return uniqueServantID;
        }
    }

    //Health in cardAsset
    private int baseHealth;
    //Health with all current buffs
    public int MaxHealth
    {
        get
        {
            return baseHealth;
        }
        set
        {
            baseHealth = value;
        }
    }

    private int health;
    public int Health
    {
        get
        {
            return health;
        }
        set
        {
            if(value > MaxHealth)
            {
                health = MaxHealth;
            }
            else if(value <= 0)
            {
                Die();
            }
            else
            {
                health = value;
            }
        }
    }

    public void BuffHealth(int buff)
    {
        MaxHealth += buff;
        Health += buff;
        new UpdateServantStatsCommand(this, -1, Health).AddToQ();
    }

    public void BuffAttack(int buff)
    {
        Attack += buff;
        new UpdateServantStatsCommand(this, Attack, -1).AddToQ();
    }

    public void SetHealth(int health)
    {
        MaxHealth = health;
        Health = health;
        new UpdateServantStatsCommand(this, -1, Health).AddToQ();
    }

    public void SetAttack(int attack)
    {
        Attack = attack;
        new UpdateServantStatsCommand(this, Attack, -1).AddToQ();
    }

    public bool CanAttack
    {
        get
        {
            bool ownersTurn = (TurnManager.Instance.WhoseTurn == owner);
            return (ownersTurn && (AttacksLeftThisTurn > 0));
        }
    }

    private bool frozen = false;
    public bool Frozen
    {
        get
        {
            return frozen;
        }
        set
        {
            frozen = value;
            GameObject g = IDHolder.GetGameObjectWIthID(uniqueServantID);
            if(g.GetComponent<OneServantManager>() != null)
            {
                g.GetComponent<OneServantManager>().Frozen = value;
            }
        }
    }


    //Attack in card asset
    private int baseAttack;
    //Attac with buffs
    public int Attack
    {
        get
        {
            return baseAttack;
        }
        set
        {
            baseAttack = value;
        }
    }

    private int attacksForOneTurn = 1;
    public int AttacksLeftThisTurn
    {
        get; set;
    }

    public ServantLogic(Player owner, CardAsset cardAsset)
    {
        this.cardAsset = cardAsset;
        baseHealth = cardAsset.MaxHealth;
        Health = cardAsset.MaxHealth;
        baseAttack = cardAsset.Attack;
        attacksForOneTurn = cardAsset.AttacksForOneTurn;
        //AttacksLeftThisTurn now 0 since cant attack if don't have Berserk
        if (cardAsset.Berserk)
        {
            AttacksLeftThisTurn = attacksForOneTurn;
        }
        this.owner = owner;
        uniqueServantID = IDFactory.GetUniqueID();
        if(cardAsset.ServantScriptName != null && cardAsset.ServantScriptName != "")
        {
            servantEffect = System.Activator.CreateInstance(System.Type.GetType(cardAsset.ServantScriptName), new System.Object[] { owner, this, cardAsset.SpecialServantAmmount }) as ServantEffect;
            servantEffect.RegisterEffect();
        }
        ServantsCreatedThisGame.Add(uniqueServantID, this);
    }

    //TODO: Remove highlighting from ServantLogic script and make it linked with commands to visual
    public void OnTurnStart()
    {
        if (Frozen)
        {
            AttacksLeftThisTurn = 0;
        }
        else
        {
            AttacksLeftThisTurn = attacksForOneTurn;
        }
    }

    public void OnTurnEnd()
    {
        if (Frozen)
        {
            Frozen = false;
        }
    }

    public void Die()
    {
        owner.table.ServantsOnTable.Remove(this);

        //TODO: cause Dying Breath (on servant death) effect if any
        if(servantEffect != null)
        {
            servantEffect.WhenServantDies();
            servantEffect.UnRegisterEffect();
        }

        new ServantDieCommand(uniqueServantID, owner).AddToQ();
        ServantsDiedThisGame.Add(this);
    }

    public void GoFace()
    {
        AttacksLeftThisTurn--;
        int targetHealthAfter = owner.otherPlayer.Health - Attack;
        new ServantAttackCommand(owner.otherPlayer.PlayerID, uniqueServantID, Health, targetHealthAfter, 0, Attack, owner).AddToQ();
        owner.otherPlayer.Health -= Attack;
    }

    public void AttackServant(ServantLogic target)
    {
        if (isThereServantWithCharm() && !target.cardAsset.Charm)
        {
            //If there are servants on enemy board with charm and the servant we are attacking does not have charm, do nothing
            //Same code as in DragServantAttack when !targetValid
            WhereIsTheCardOrServant whereIsServant = IDHolder.GetGameObjectWIthID(ID).GetComponent<WhereIsTheCardOrServant>();
            if (owner.tag.Contains("Bottom")){
                whereIsServant.State = VisualStates.BottomTable;
            }
            else
            {
                whereIsServant.State = VisualStates.TopTable;
            }
            whereIsServant.SetTableSortingOrder();
        }
        else
        {
            AttacksLeftThisTurn--;
            int targetHealthAfter = target.Health - Attack;
            //If the attacking servant has venomous and dealt damage, set the targets health to 0
            if (cardAsset.Venomous && targetHealthAfter != target.Health)
            {
                targetHealthAfter = target.Health - target.Health;
            }
            int attackerHealthAfter = Health - target.Attack;
            if (target.cardAsset.Venomous && attackerHealthAfter != this.Health)
            {
                attackerHealthAfter = this.Health - this.Health;
            }
            new ServantAttackCommand(target.uniqueServantID, uniqueServantID, attackerHealthAfter, targetHealthAfter, target.Attack, Attack, owner).AddToQ();

            if (cardAsset.Vampire)
            {
                new DealDamageCommand(owner.ID, -Attack, owner.Health + Attack).AddToQ();
                owner.Health += Attack;
            }
            if (target.cardAsset.Vampire)
            {
                new DealDamageCommand(target.owner.ID, -target.Attack, target.owner.Health + target.Attack).AddToQ();
                target.owner.Health += Attack;
            }

            target.Health = targetHealthAfter;
            Health = attackerHealthAfter;
        }
    }

    public void AttackServantWithID(int uniqueServantID)
    {
        ServantLogic target = ServantLogic.ServantsCreatedThisGame[uniqueServantID];
        AttackServant(target);
    }

    private bool isThereServantWithCharm()
    {
        bool servantWithCharm = false;
        foreach (ServantLogic sl in owner.otherPlayer.table.ServantsOnTable)
        {
            if (sl.cardAsset.Charm)
            {
                servantWithCharm = true;
            }
        }
        return servantWithCharm;
    }
}
