﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonBreath : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 4, ICharacter target = null)
    {
        ServantLogic[] ServantsToDamage = TurnManager.Instance.WhoseTurn.otherPlayer.table.ServantsOnTable.ToArray();
        foreach(ServantLogic sl in ServantsToDamage)
        {
            new DealDamageCommand(sl.ID, specialAmmount, healthAfter: sl.Health - specialAmmount).AddToQ();
            sl.Health -= specialAmmount;
        }
    }
}
