﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class PhoenixBreath : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 5, ICharacter target = null)
    {
        int adjecentDamage = 2;
        List<ServantLogic> ServantsOnTable = TurnManager.Instance.WhoseTurn.otherPlayer.table.ServantsOnTable;
        int targetPosition = 0;
        for(int i = 0; i < ServantsOnTable.Count; i++)
        {
            if(ServantsOnTable[i].ID == target.ID)
            {
                targetPosition = i;
                break;
            }
        }
        if(targetPosition == 0)
        {
            //Checks if element at index exists
            if(ServantsOnTable.ElementAtOrDefault(1) != null)
            {
                new DealDamageCommand(ServantsOnTable[1].ID, adjecentDamage, healthAfter: ServantsOnTable[1].Health - adjecentDamage).AddToQ();
                ServantsOnTable[1].Health -= adjecentDamage;
            }
            new DealDamageCommand(target.ID, specialAmmount, healthAfter: target.Health - specialAmmount).AddToQ();
            target.Health -= specialAmmount;
        }
        else
        {
            if (ServantsOnTable.ElementAtOrDefault(targetPosition+1) != null)
            {
                new DealDamageCommand(ServantsOnTable[targetPosition + 1].ID, adjecentDamage, healthAfter: ServantsOnTable[targetPosition + 1].Health - adjecentDamage).AddToQ();
                ServantsOnTable[targetPosition + 1].Health -= adjecentDamage;
            }
            new DealDamageCommand(ServantsOnTable[targetPosition - 1].ID, adjecentDamage, healthAfter: ServantsOnTable[targetPosition - 1].Health - adjecentDamage).AddToQ();
            ServantsOnTable[targetPosition - 1].Health -= adjecentDamage;

            new DealDamageCommand(target.ID, specialAmmount, healthAfter: target.Health - specialAmmount).AddToQ();
            target.Health -= specialAmmount;
        }
    }
}
