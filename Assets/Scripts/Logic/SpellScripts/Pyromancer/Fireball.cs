﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 6, ICharacter target = null)
    {
        new DealDamageCommand(target.ID, specialAmmount, healthAfter: target.Health - specialAmmount).AddToQ();
        target.Health -= specialAmmount;
    }
}
