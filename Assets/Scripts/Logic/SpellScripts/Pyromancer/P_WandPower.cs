﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P_WandPower : SpellEffect
{
    public int damage = 3;
    public override void ActivateEffect(int specialAmmount = 3, ICharacter target = null)
    {
        Player enemy = TurnManager.Instance.WhoseTurn.otherPlayer;
        new DealDamageCommand(enemy.ID, damage, enemy.Health - damage).AddToQ();
        enemy.Health -= damage;
    }
}
