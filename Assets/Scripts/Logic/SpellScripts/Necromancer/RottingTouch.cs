﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RottingTouch : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 3, ICharacter target = null)
    {
        Player owner = TurnManager.Instance.WhoseTurn;
        new DealDamageCommand(target.ID, specialAmmount, target.Health - specialAmmount).AddToQ();
        new DealDamageCommand(owner.ID, -specialAmmount, owner.Health + specialAmmount).AddToQ();
        target.Health -= specialAmmount;
        owner.Health += specialAmmount;
    }
}
