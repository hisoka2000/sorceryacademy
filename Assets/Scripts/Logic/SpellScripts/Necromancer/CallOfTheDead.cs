﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallOfTheDead : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 3, ICharacter target = null)
    {
        Player owner = TurnManager.Instance.WhoseTurn;
        if(owner.table.ServantsOnTable.Count + specialAmmount > 6)
        {
            specialAmmount = 6 - owner.table.ServantsOnTable.Count;
        }

        if(specialAmmount > 0)
        {
            CardAsset SkeletonAsset = CardAssetsList.CardAssets["Skeleton"];
            CardLogic Skeleton = new CardLogic(SkeletonAsset);

            for (int i = 0; i < specialAmmount; i++)
            {
                owner.SummonServantNotFromHand(Skeleton, 0);
            }
        }
    }
}
