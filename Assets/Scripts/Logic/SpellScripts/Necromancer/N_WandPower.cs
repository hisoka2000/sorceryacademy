﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class N_WandPower : SpellEffect
{
    public int damage = 2;
    public override void ActivateEffect(int specialAmmount = 2, ICharacter target = null)
    {
        Player owner = TurnManager.Instance.WhoseTurn;
        new DealDamageCommand(owner.ID, damage, owner.Health - damage).AddToQ();
        owner.Health -= damage;
        owner.DrawACard();
    }
}
