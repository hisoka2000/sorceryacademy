﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathCoil : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 3, ICharacter target = null)
    {
        Player owner = TurnManager.Instance.WhoseTurn;
        new DealDamageCommand(target.ID, specialAmmount, target.Health - specialAmmount).AddToQ();
        if(target.Health - specialAmmount <= 0)
        {
            owner.DrawACard(true);
            owner.DrawACard(true);
        }
        target.Health -= specialAmmount;
    }
}
