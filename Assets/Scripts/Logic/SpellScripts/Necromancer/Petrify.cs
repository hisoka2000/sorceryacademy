﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Petrify : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 1, ICharacter target = null)
    {
        TurnManager.Instance.nextPlayerSkipsTurn = true;
    }
}
