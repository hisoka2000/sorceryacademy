﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 1, ICharacter target = null)
    {
        TurnManager.Instance.WhoseTurn.GetBonusMana(specialAmmount);
    }
}
