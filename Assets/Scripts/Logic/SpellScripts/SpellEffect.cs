﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellEffect
{
    public virtual void ActivateEffect(int specialAmmount = 0, ICharacter target = null)
    {
        Debug.Log("No spell with this name");
    }
}
