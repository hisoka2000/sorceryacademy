﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class O_WandPower : SpellEffect
{
    public int damage = 1;
    public override void ActivateEffect(int specialAmmount = 1, ICharacter target = null)
    {
        ServantLogic[] EnemyServants = TurnManager.Instance.WhoseTurn.otherPlayer.table.ServantsOnTable.ToArray();
        if(EnemyServants.Length > 0)
        {
            ServantLogic randomServant = EnemyServants[Random.Range(0, EnemyServants.Length)];
            new DealDamageCommand(randomServant.ID, damage, healthAfter: randomServant.Health - damage).AddToQ();
            randomServant.Health -= damage;
            randomServant.Frozen = true;
        }

    }
}
