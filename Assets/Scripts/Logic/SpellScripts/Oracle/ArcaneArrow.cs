﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcaneArrow : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 2, ICharacter target = null)
    {
        int frozenDamage = 6;
        ServantLogic servant = ServantLogic.ServantsCreatedThisGame[target.ID];
        if (servant.Frozen)
        {
            new DealDamageCommand(target.ID, frozenDamage, target.Health - frozenDamage).AddToQ();
            target.Health -= frozenDamage;
        }
        else
        {
            new DealDamageCommand(target.ID, specialAmmount, target.Health - specialAmmount).AddToQ();
            target.Health -= specialAmmount;
        }
    }
}
