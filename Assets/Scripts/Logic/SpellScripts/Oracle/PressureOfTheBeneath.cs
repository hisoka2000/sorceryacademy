﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressureOfTheBeneath : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 5, ICharacter target = null)
    {
        Player owner = TurnManager.Instance.WhoseTurn;
        int numberOfServantsSummoned = 0;
        if(owner.table.ServantsOnTable.Count + specialAmmount > 6)
        {
            specialAmmount = 6 - owner.table.ServantsOnTable.Count;
        }

        if(specialAmmount > 0)
        {
            for (int i = 0; i < owner.deck.cards.Count; i++)
            {
                if (owner.deck.cards[i].MaxHealth == 0)
                {
                    //if its a spell
                    continue;
                }
                else
                {
                    //if its a servant
                    CardLogic Servant = new CardLogic(owner.deck.cards[i]);
                    owner.SummonServantNotFromHand(Servant, 0);
                    owner.deck.cards.RemoveAt(i);
                    i--;
                    numberOfServantsSummoned++;
                    if (numberOfServantsSummoned == specialAmmount)
                    {
                        break;
                    }
                }
            }
        }
    }
}
