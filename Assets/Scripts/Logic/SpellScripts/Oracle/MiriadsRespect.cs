﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiriadsRespect : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 6, ICharacter target = null)
    {
        ServantLogic servant = ServantLogic.ServantsCreatedThisGame[target.ID];
        servant.BuffHealth(specialAmmount);
        servant.BuffAttack(specialAmmount);
        servant.Frozen = true;
    }
}
