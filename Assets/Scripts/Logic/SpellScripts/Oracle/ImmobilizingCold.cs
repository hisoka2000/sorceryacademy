﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImmobilizingCold : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 2, ICharacter target = null)
    {
        Player enemy = TurnManager.Instance.WhoseTurn.otherPlayer;
        ServantLogic[] ServantsToDamage = enemy.table.ServantsOnTable.ToArray();
        foreach (ServantLogic sl in ServantsToDamage)
        {
            new DealDamageCommand(sl.ID, specialAmmount, healthAfter: sl.Health - specialAmmount).AddToQ();
            sl.Frozen = true;
            sl.Health -= specialAmmount;
        }
        new DealDamageCommand(enemy.ID, specialAmmount, enemy.Health - specialAmmount).AddToQ();
        enemy.Health -= specialAmmount;
    }
}
