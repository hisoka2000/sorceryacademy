﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_WandPower : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 0, ICharacter target = null)
    {
        Player owner = TurnManager.Instance.WhoseTurn;
        if (owner.table.ServantsOnTable.Count != 0)
        {
            //Target ally servants
            int randomAllyServant = Random.Range(0, owner.table.ServantsOnTable.Count);
            ServantLogic randomServant = owner.table.ServantsOnTable[randomAllyServant];
            randomServant.BuffAttack(2);
            randomServant.BuffHealth(2);
        }
    }
}
