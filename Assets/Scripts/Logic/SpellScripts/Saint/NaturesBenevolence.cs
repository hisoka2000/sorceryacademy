﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaturesBenevolence : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 4, ICharacter target = null)
    {
        ServantLogic servant = ServantLogic.ServantsCreatedThisGame[target.ID];
        servant.BuffHealth(specialAmmount);
        servant.BuffAttack(specialAmmount);
    }
}
