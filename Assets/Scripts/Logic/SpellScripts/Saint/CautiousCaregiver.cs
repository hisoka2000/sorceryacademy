﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CautiousCaregiver : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 0, ICharacter target = null)
    {
        Player owner = TurnManager.Instance.WhoseTurn;
        ServantLogic servant = ServantLogic.ServantsCreatedThisGame[target.ID];
        servant.BuffHealth(3);
        servant.BuffAttack(1);
        owner.DrawACard();
    }
}
