﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeOfLife : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 0, ICharacter target = null)
    {
        Player owner = TurnManager.Instance.WhoseTurn;
        ServantLogic[] ServantsToHeal = owner.table.ServantsOnTable.ToArray();
        int ammountToDamage = 0;
        foreach (ServantLogic sl in ServantsToHeal)
        {
            int ammountToHeal = sl.MaxHealth - sl.Health;
            new DealDamageCommand(sl.ID, -ammountToHeal, healthAfter: sl.MaxHealth).AddToQ();
            sl.Health = sl.MaxHealth;
            ammountToDamage += ammountToHeal;
        }
        new DealDamageCommand(owner.otherPlayer.ID, ammountToDamage, owner.otherPlayer.Health - ammountToDamage).AddToQ();
        owner.otherPlayer.Health -= ammountToDamage;
    }
}
