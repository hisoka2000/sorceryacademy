﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeGiant : SpellEffect
{
    public override void ActivateEffect(int specialAmmount = 2, ICharacter target = null)
    {
        //It is important to reset values of card asset as it stays the same even after the game is over. TODO: Rewrite logic to not modify cardAssets
        Player owner = TurnManager.Instance.WhoseTurn;
        if(owner.table.ServantsOnTable.Count < 6)
        {
            int cardsPlayedThisTurn = owner.cardsPlayedThisTurn;
            CardAsset RootedGiantAsset = CardAssetsList.CardAssets["Rooted Giant"];
            int initialAttack = RootedGiantAsset.Attack;
            int initialHealth = RootedGiantAsset.MaxHealth;

            if (cardsPlayedThisTurn == 0)
            {
                Debug.LogError("Unwanted behaviour of cardsPlayedThisTurn = 0");
            }
            else
            {
                RootedGiantAsset.Attack += specialAmmount * cardsPlayedThisTurn;
                RootedGiantAsset.MaxHealth += specialAmmount * cardsPlayedThisTurn;
            }
            CardLogic TreeGiant = new CardLogic(RootedGiantAsset);
            owner.SummonServantNotFromHand(TreeGiant, 0);

            RootedGiantAsset.Attack = initialAttack;
            RootedGiantAsset.MaxHealth = initialHealth;
        }
    }
}
