﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TurnManager : MonoBehaviour
{
    public CardAsset coinCard;
    //Used when we want the next player after end turn to skip their turn immidiately. For example, used in Petrify spell
    public bool nextPlayerSkipsTurn = false;

    private RopeTimer timer;

    public static TurnManager Instance;

    private Player whoseTurn;
    public Player WhoseTurn
    {
        get
        {
            return whoseTurn;
        }

        set
        {
            whoseTurn = value;
            timer.StartTimer();
            GlobalSettings.Instance.EnableEndTurnButtonOnStart(whoseTurn);

            TurnMaker tm = whoseTurn.GetComponent<TurnMaker>();
            //calls players OnTurnStart
            tm.OnTurnStart();
            if(tm is PlayerTurnMaker)
            {
                whoseTurn.HighlightPlayableCards();
            }
            //remove highlights for enemy
            whoseTurn.otherPlayer.HighlightPlayableCards(true);
        }
    }

    void Awake()
    {
        Instance = this;
        timer = GetComponent<RopeTimer>();
    }

    void Start()
    {
        OnGameStart();
    }

    //TODO: Change this and accompanying visual so that on start the cards are not dealt one by one (Maybe add mulligan ability as well)
    public void OnGameStart()
    {
        CardLogic.CardsCreatedThisGame.Clear();
        ServantLogic.ServantsCreatedThisGame.Clear();

        foreach(Player player in Player.Players)
        {
            player.ManaThisTurn = 0;
            player.ManaLeft = 0;
            player.LoadCharacterInfoFromAsset();
            //TEST comment next line out
            player.TransmitInfoAboutPlayerToVisual();
            player.playerArea.playerDeck.CardsInDeck = player.deck.cards.Count;
            //Move both player illustrations to center
            player.playerArea.playerIllustration.transform.position = player.playerArea.startPlayerIllustrationSpot.position;
        }

        Sequence s = DOTween.Sequence();
        s.Append(Player.Players[0].playerArea.playerIllustration.transform.DOMove(Player.Players[0].playerArea.illustrationPosition.position, 1f).SetEase(Ease.InQuad));
        s.Insert(0f, Player.Players[1].playerArea.playerIllustration.transform.DOMove(Player.Players[1].playerArea.illustrationPosition.position, 1f).SetEase(Ease.InQuad));
        s.PrependInterval(3f);
        s.OnComplete(() =>
        {
            // Randomly decide who starts the game
            int rnd = Random.Range(0, 2);

            Player whoGoesFirst = Player.Players[rnd];
            Player whoGoesSecond = whoGoesFirst.otherPlayer;

            // First player draws 4 cards while the second player draws 5
            int initDraw = 4;
            for (int i = 0; i < initDraw; i++)
            {
                // second player draws a card
                whoGoesSecond.DrawACard(true);
                // first player draws a card
                whoGoesFirst.DrawACard(true);
            }
            // add the fifth (initDraw+1th) card to second players hand
            whoGoesSecond.DrawACard(true);
            // and add a coin to second players hand
            whoGoesSecond.GetCardNotFromDeck(coinCard);
            new StartATurnCommand(whoGoesFirst).AddToQ();
        });
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            EndTurn();
        }
    }

    public void EndTurn()
    {
        timer.StopTimer();
        //send all commands in the end of current players turn
        whoseTurn.OnTurnEnd();
        //Check if the next player has to skip their turn, if yes then the current player again starts their turn and nextPlayerSkipsTurn is set to false
        if (nextPlayerSkipsTurn)
        {
            new StartATurnCommand(whoseTurn).AddToQ();
            nextPlayerSkipsTurn = false;
        }
        else
        {
            new StartATurnCommand(whoseTurn.otherPlayer).AddToQ();
        }
    }

    public void EndTurnTest()
    {
        timer.StartTimer();
        GlobalSettings.Instance.ropeGameObjectInGame.GetComponent<RopeTimerVisual>().StartTimer(30);
    }

    public void StopTheTimer()
    {
        timer.StopTimer();
    }
}
