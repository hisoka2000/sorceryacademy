﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Responsible for storing all CardAssets of our game in logic
public class CardAssetsList : MonoBehaviour
{
    public List<CardAsset> allAssets = new List<CardAsset>();
    public static Dictionary<string, CardAsset> CardAssets = new Dictionary<string, CardAsset>();

    void Awake()
    {
        foreach(CardAsset ca in allAssets)
        {
            CardAssets.Add(ca.cardName, ca);
        }
    }
}
