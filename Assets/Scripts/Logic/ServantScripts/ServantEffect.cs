﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ServantEffect
{
    protected Player owner;
    protected ServantLogic servant;
    protected int specialAmmount;

    public ServantEffect(Player owner, ServantLogic servant, int specialAmmount)
    {
        this.owner = owner;
        this.servant = servant;
        this.specialAmmount = specialAmmount;
    }

    //Methods for effects that listen to events
    //Currently events are, in Player, EndTurnEvent, StartTurnEvent, SpellPlayedEvent and ServantPlayedEvent
    public virtual void RegisterEffect() { }
    public virtual void UnRegisterEffect() { }
    public virtual void CauseEffect() { }

    //Divination
    public virtual void WhenServantIsPlayed(int specialServantAmmount, ICharacter target) { }

    //Dying Breath
    public virtual void WhenServantDies() { }
}
