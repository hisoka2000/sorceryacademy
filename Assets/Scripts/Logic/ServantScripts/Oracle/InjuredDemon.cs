﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InjuredDemon : ServantEffect
{
    public InjuredDemon(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }

    public override void WhenServantIsPlayed(int specialServantAmmount, ICharacter target)
    {
        Player enemy = TurnManager.Instance.WhoseTurn.otherPlayer;
        ServantLogic[] ServantsToFreeze = enemy.table.ServantsOnTable.ToArray();
        foreach (ServantLogic sl in ServantsToFreeze)
        {
            sl.Frozen = true;
        }
    }
}
