﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AquaticMaiden : ServantEffect
{
    public AquaticMaiden(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }

    public override void WhenServantIsPlayed(int specialServantAmmount, ICharacter target)
    {
        Player enemy = TurnManager.Instance.WhoseTurn.otherPlayer;
        ServantLogic[] ServantsToDamage = enemy.table.ServantsOnTable.ToArray();
        foreach (ServantLogic sl in ServantsToDamage)
        {
            new DealDamageCommand(sl.ID, specialAmmount, healthAfter: sl.Health - specialAmmount).AddToQ();
            sl.Frozen = true;
            sl.Health -= specialAmmount;
        }
    }
}
