﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadyOfTheLake : ServantEffect
{
    public LadyOfTheLake(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }
    public override void WhenServantIsPlayed(int specialServantAmmount, ICharacter target)
    {
        new DealDamageCommand(target.ID, -specialAmmount, target.Health + specialAmmount).AddToQ();
        target.Health += specialAmmount;
    }
}
