﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlustyWorp : ServantEffect
{
    public GlustyWorp(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }

    public override void WhenServantIsPlayed(int specialServantAmmount, ICharacter target)
    {
        int ammountToBuff = 0;
        ServantLogic[] ServantsToDamage = TurnManager.Instance.WhoseTurn.otherPlayer.table.ServantsOnTable.ToArray();
        foreach (ServantLogic sl in ServantsToDamage)
        {
            new DealDamageCommand(sl.ID, specialServantAmmount, healthAfter: sl.Health - specialServantAmmount).AddToQ();
            sl.Health -= specialServantAmmount;
            ammountToBuff += specialServantAmmount;
        }
        servant.BuffHealth(ammountToBuff);
    }
}
