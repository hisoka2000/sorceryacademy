﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServantOfTheForest : ServantEffect
{
    public ServantOfTheForest(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }

    public override void WhenServantIsPlayed(int specialServantAmmount, ICharacter target)
    {
        int ammountToHeal = target.Health;
        new DealDamageCommand(target.ID, target.Health, 0).AddToQ();
        target.Health = 0;

        new DealDamageCommand(owner.ID, -ammountToHeal, owner.Health + ammountToHeal).AddToQ();
        owner.Health += ammountToHeal;
    }
}
