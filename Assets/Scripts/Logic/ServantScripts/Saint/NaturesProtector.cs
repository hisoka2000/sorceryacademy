﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaturesProtector : ServantEffect
{
    public NaturesProtector(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }

    public override void WhenServantIsPlayed(int specialServantAmmount, ICharacter target)
    {
        int ammountToBuff = 0;

        foreach (ServantLogic sl in ServantLogic.ServantsDiedThisGame)
        {
            if(sl.cardAsset.characterAsset == null)
            {
                continue;
            }
            else if (sl.cardAsset.characterAsset.ClassName == "V" && sl.owner == owner)
            {
                ammountToBuff += specialServantAmmount;
            }
        }

        servant.BuffAttack(ammountToBuff);
        servant.BuffHealth(ammountToBuff);
    }
}
