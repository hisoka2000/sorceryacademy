﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagmaDragon : ServantEffect
{
    public MagmaDragon(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }

    public override void WhenServantIsPlayed(int specialServantAmmount, ICharacter target)
    {
        ServantLogic[] EnemyServants = TurnManager.Instance.WhoseTurn.otherPlayer.table.ServantsOnTable.ToArray();
        ServantLogic[] AllyServants = TurnManager.Instance.WhoseTurn.table.ServantsOnTable.ToArray();
        foreach (ServantLogic sl in EnemyServants)
        {
            new DealDamageCommand(sl.ID, sl.Health, healthAfter: sl.Health - sl.Health).AddToQ();
            sl.Health -= sl.Health;
        }
        foreach (ServantLogic sl in AllyServants)
        {
            if(sl != servant)
            {
                new DealDamageCommand(sl.ID, sl.Health, healthAfter: sl.Health - sl.Health).AddToQ();
                sl.Health -= sl.Health;
            }
        }
        new DealDamageCommand(owner.ID, specialServantAmmount, owner.Health - specialServantAmmount).AddToQ();
        owner.Health -= specialServantAmmount;
    }
}
