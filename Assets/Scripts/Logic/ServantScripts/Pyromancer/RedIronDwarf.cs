﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedIronDwarf : ServantEffect
{
    public RedIronDwarf(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }

    public override void RegisterEffect()
    {
        owner.EndTurnEvent += CauseEffect;
    }

    public override void UnRegisterEffect()
    {
        owner.EndTurnEvent -= CauseEffect;
    }

    public override void CauseEffect()
    {
        List<ServantLogic> servants = owner.table.ServantsOnTable;
        int randomServant = Random.Range(0, servants.Count);
        servants[randomServant].BuffAttack(specialAmmount);
        servants[randomServant].BuffHealth(specialAmmount);
    }
}
