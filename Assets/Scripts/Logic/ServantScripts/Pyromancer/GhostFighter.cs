﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostFighter : ServantEffect
{
    public GhostFighter(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }

    public override void WhenServantIsPlayed(int specialAmmount, ICharacter target)
    {
        //Deal 1 damage to everything
        ServantLogic[] EnemyServants = TurnManager.Instance.WhoseTurn.otherPlayer.table.ServantsOnTable.ToArray();
        ServantLogic[] AllyServants = TurnManager.Instance.WhoseTurn.table.ServantsOnTable.ToArray();
        Player bottomPlayer = GlobalSettings.Instance.BottomPlayer;
        Player topPlayer = GlobalSettings.Instance.TopPlayer;

        //Deal damage to all enemy servants
        foreach(ServantLogic sl in EnemyServants)
        {
            new DealDamageCommand(sl.ID, specialAmmount, healthAfter: sl.Health - specialAmmount).AddToQ();
            sl.Health -= specialAmmount;
        }
        //Deal damage to all ally servants
        foreach (ServantLogic sl in AllyServants)
        {
            new DealDamageCommand(sl.ID, specialAmmount, healthAfter: sl.Health - specialAmmount).AddToQ();
            sl.Health -= specialAmmount;
        }
        //Deal damage to ally and enemy players
        new DealDamageCommand(bottomPlayer.ID, specialAmmount, healthAfter: bottomPlayer.Health - specialAmmount).AddToQ();
        new DealDamageCommand(topPlayer.ID, specialAmmount, healthAfter: topPlayer.Health - specialAmmount).AddToQ();
        bottomPlayer.Health -= specialAmmount;
        topPlayer.Health -= specialAmmount;
    }
}
