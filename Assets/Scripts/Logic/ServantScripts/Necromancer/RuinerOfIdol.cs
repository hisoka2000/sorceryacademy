﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuinerOfIdol : ServantEffect
{
    public RuinerOfIdol(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }

    public override void WhenServantIsPlayed(int specialServantAmmount, ICharacter target)
    {
        Player enemy = servant.owner.otherPlayer;
        ServantLogic[] EnemyServants = enemy.table.ServantsOnTable.ToArray();
        List<ServantLogic> ServantsToHit = new List<ServantLogic>();

        for(int i = 0; i < EnemyServants.Length; i++)
        {
            if(EnemyServants[i].Attack >= specialAmmount)
            {
                ServantsToHit.Add(EnemyServants[i]);
            }
        }

        if(ServantsToHit.Count == 0)
        {

        }
        else
        {
            int randomEnemyIndex = Random.Range(0, ServantsToHit.Count);
            new DealDamageCommand(ServantsToHit[randomEnemyIndex].ID, ServantsToHit[randomEnemyIndex].Health, 0).AddToQ();
            ServantsToHit[randomEnemyIndex].Health -= ServantsToHit[randomEnemyIndex].Health;
        }
    }
}
