﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoyalSubordinate : ServantEffect
{
    public LoyalSubordinate(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }

    public override void WhenServantDies()
    {
        Player enemy = servant.owner.otherPlayer;
        for(int i = 0; i < 2; i++)
        {
            ServantLogic[] EnemyServants = enemy.table.ServantsOnTable.ToArray();
            if (EnemyServants.Length == 0)
            {
                //target is player
                new DealDamageCommand(enemy.ID, specialAmmount, enemy.Health - specialAmmount).AddToQ();
                enemy.Health -= specialAmmount;
            }
            else
            {
                //Pick random target from servants and player
                int randomEnemyIndex = Random.Range(0, EnemyServants.Length + 1);
                if (randomEnemyIndex == EnemyServants.Length)
                {
                    //target is player
                    new DealDamageCommand(enemy.ID, specialAmmount, enemy.Health - specialAmmount).AddToQ();
                    enemy.Health -= specialAmmount;
                }
                else
                {
                    //target is servant
                    ServantLogic randomServant = EnemyServants[randomEnemyIndex];
                    new DealDamageCommand(randomServant.ID, specialAmmount, healthAfter: randomServant.Health - specialAmmount).AddToQ();
                    randomServant.Health -= specialAmmount;
                }
            }
        }
    }
}
