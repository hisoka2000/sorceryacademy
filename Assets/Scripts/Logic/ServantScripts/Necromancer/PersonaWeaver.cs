﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonaWeaver : ServantEffect
{
    public PersonaWeaver(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }

    public override void WhenServantIsPlayed(int specialServantAmmount, ICharacter target)
    {
        ServantLogic targetServant = ServantLogic.ServantsCreatedThisGame[target.ID];
        int targetAttack = targetServant.Attack;
        int targetHealth = targetServant.Health;

        targetServant.SetAttack(servant.Attack);
        targetServant.SetHealth(servant.Health);

        servant.SetAttack(targetAttack);
        servant.SetHealth(targetHealth);
    }
}
