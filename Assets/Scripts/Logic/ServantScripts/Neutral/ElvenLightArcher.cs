﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElvenLightArcher : ServantEffect
{
    public ElvenLightArcher(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }

    public override void WhenServantIsPlayed(int specialServantAmmount, ICharacter target)
    {
        Player enemy = TurnManager.Instance.WhoseTurn.otherPlayer;

        new DealDamageCommand(enemy.ID, specialServantAmmount, enemy.Health - specialServantAmmount).AddToQ();
        enemy.Health -= specialServantAmmount;
    }
}
