﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shenlong : ServantEffect
{
    public Shenlong(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }

    public override void WhenServantDies()
    {
        Player enemy = servant.owner.otherPlayer;
        ServantLogic[] EnemyServants = enemy.table.ServantsOnTable.ToArray();
        List<ServantLogic> enemiesToHit = new List<ServantLogic>();

        //Target only those servants which are currently alive
        foreach(ServantLogic sl in EnemyServants)
        {
            if(sl.Health > 0)
            {
                enemiesToHit.Add(sl);
            }
        }
        if (enemiesToHit.Count == 0)
        {
            //target is player
            new DealDamageCommand(enemy.ID, specialAmmount, enemy.Health - specialAmmount).AddToQ();
            enemy.Health -= specialAmmount;
        }
        else
        {
            //Pick random target from servants and player
            int randomEnemyIndex = Random.Range(0, enemiesToHit.Count + 1);
            if (randomEnemyIndex == enemiesToHit.Count)
            {
                //target is player
                new DealDamageCommand(enemy.ID, specialAmmount, enemy.Health - specialAmmount).AddToQ();
                enemy.Health -= specialAmmount;
            }
            else
            {
                //target is servant
                ServantLogic randomServant = enemiesToHit[randomEnemyIndex];
                new DealDamageCommand(randomServant.ID, specialAmmount, healthAfter: randomServant.Health - specialAmmount).AddToQ();
                randomServant.Health -= specialAmmount;
            }
        }
    }
}
