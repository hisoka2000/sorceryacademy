﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CockyMinx : ServantEffect
{
    public CockyMinx(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }

    public override void WhenServantIsPlayed(int specialServantAmmount, ICharacter target)
    {
        new DealDamageCommand(target.ID, specialServantAmmount, target.Health - specialServantAmmount).AddToQ();
        target.Health -= specialServantAmmount;
    }
}
