﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacelessCheetah : ServantEffect
{
    public FacelessCheetah(Player owner, ServantLogic servant, int specialAmmount = 1) : base(owner, servant, specialAmmount) { }

    public override void WhenServantDies()
    {
        foreach (ServantLogic sl in ServantLogic.ServantsDiedThisGame)
        {
            if (sl.cardAsset.cardName == "Demonic Cheetah" && sl.owner == owner)
            {
                CardAsset StoneGiantAsset = CardAssetsList.CardAssets["Stone Giant"];
                CardLogic StoneGiant = new CardLogic(StoneGiantAsset);
                owner.SummonServantNotFromHand(StoneGiant, 0);
                break;
            }
        }
    }
}
