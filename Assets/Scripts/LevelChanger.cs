﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class LevelChanger : MonoBehaviour
{
    public Animator animator;
    private int levelToLoad;
    private AudioSource audioSource;

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FadeToLevel(int levelIndex)
    {
        audioSource = GameObject.Find("Main Camera").GetComponent<AudioSource>();
        audioSource.DOFade(0, 1f);
        if(levelIndex == 0)
        {
            Destroy(GameObject.Find("GameData"));
            IDFactory.ResetIDs();
            IDHolder.ClearIDHoldersList();
            Command.CommandQ.Clear();
            Command.CommandExecutionComplete();
        }
        levelToLoad = levelIndex;
        animator.SetTrigger("FadeOut");
    }

    public void OnFadeComeplete()
    {
        SceneManager.LoadScene(levelToLoad);
    }
}
