﻿using System.Collections;
using UnityEngine;
using DG.Tweening;

public class LightFlutter : MonoBehaviour
{
    private Light myLight;
    private float initialIntensity; //Initial light intensity
    public float margin = 10f;      //How much should the intensity flutter
    public float timeDelta = 1f;   //How much time should the tweening take
    private float time; //Random time = timeDelta +- 0.1
    // Start is called before the first frame update
    void Start()
    {
        myLight = gameObject.GetComponent<Light>();
        initialIntensity = myLight.intensity;
    }

    // Update is called once per frame
    void Update()
    {
        time = Random.Range(timeDelta - 0.1f, timeDelta + 0.1f);
        if(myLight.intensity == initialIntensity)
        {
            myLight.DOIntensity(initialIntensity + margin, time).SetEase(Ease.OutSine);
        } 
        else if(myLight.intensity == initialIntensity + margin)
        {
            myLight.DOIntensity(initialIntensity, time).SetEase(Ease.OutSine);
        }

    }
}
