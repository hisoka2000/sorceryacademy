﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EqualCards : MonoBehaviour
{
    //Used to make cards and servants (Objects) have equal distance to each other

    public Transform[] objects;

    void Awake()
    {
        //Position of first and last objects to calculate the needed average distance
        Vector3 firstObjectPos = objects[0].transform.position;
        Vector3 lastObjectPos = objects[objects.Length - 1].transform.position;

        //Middle object index and position to appropriately control the y position of cards for rotation
        int middleObjectIndex = (int)Mathf.Ceil((objects.Length - 1) / 2);
        Vector3 middleObjectPos = objects[middleObjectIndex].transform.position;

        //Rotation of first and last objects to calculate the needed average rotation
        Vector3 firstObjectRot = objects[0].transform.eulerAngles;
        Vector3 lastObjectRot = objects[objects.Length - 1].transform.eulerAngles;

        //Average x, y and z distance calculation
        float xDist = (lastObjectPos.x - firstObjectPos.x) / (float)(objects.Length - 1);
        float yDist = (lastObjectPos.y - firstObjectPos.y) / (float)(objects.Length - 1);
        float zDist = (lastObjectPos.z - firstObjectPos.z) / (float)(objects.Length - 1);

        //Average z rotation calculation
        float zRot = Mathf.Abs(lastObjectRot.z - (firstObjectRot.z - 360)) / (float)(objects.Length - 1);

        //Average y position to shift
        float yPos = Mathf.Abs(middleObjectPos.y - firstObjectPos.y) / (float)(objects.Length - 1);

        //Rotation vector based on zRot
        Vector3 Rot = new Vector3(0, 0, zRot);
        //Position vector based on yPos
        Vector3 Pos = new Vector3(0, yPos, 0);
        //Distance vector based on xDist, yDist and zDist
        Vector3 Dist = new Vector3(xDist, yDist, zDist);


        //TODO change if statement so that when player at top is AI the cards are rotated
        if(GetComponentInParent<HandVisual>().owner == AreaPosition.Top)
        {
            for (int i = 1; i < objects.Length; i++)
            {
                objects[i].transform.position = objects[i - 1].transform.position + Dist;
            }
        }
        else
        {
            for (int i = 1; i < objects.Length; i++)
            {
                objects[i].transform.eulerAngles = objects[i - 1].transform.eulerAngles + Rot;

                if (i < middleObjectIndex)
                {
                    objects[i].transform.position = objects[i - 1].transform.position + Dist + Pos;
                }
                else if (i > middleObjectIndex)
                {
                    objects[i].transform.position = objects[i - 1].transform.position + Dist - Pos;
                }
                else
                {
                    objects[i].transform.position = objects[i - 1].transform.position + Dist;
                }
            }
        }
    }
}
