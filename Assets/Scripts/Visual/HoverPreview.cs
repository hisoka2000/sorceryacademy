﻿using System.Collections;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class HoverPreview : MonoBehaviour
{
    //Public fields

    //Whenever previewing card from hand need to hide the card gameobject in the players hand
    public GameObject turnOffWhenPreviewing;
    //Position of preview relative to card ((0, 0, 0) for exact same position as card)
    public Vector3 targetPosition;
    //Scale of card preview relative to card (1 for exact same scale as card)
    public float targetScale;
    //Seconds to wait till the preview appears
    public float secondsTillPreview = 0;
    public GameObject previewGameObject;
    //Activate script on awake. Need to not activate it when dragging a card and hovering over some other card.
    public bool activateInAwake = false;


    //Private fields

    private static HoverPreview currentlyViewing = null;

    //Properties
    private static bool previewsAllowed = true;
    public static bool PreviewsAllowed
    {
        get
        {
            return previewsAllowed;
        }
        set
        {
            previewsAllowed = value;
            if (!previewsAllowed)
            {
                stopAllPreviews();
            }
        }
    }

    private bool thisPreviewEnabled = false;
    public bool ThisPreviewEnabled
    {
        get
        {
            return thisPreviewEnabled;
        }
        set
        {
            thisPreviewEnabled = value;
            if (!thisPreviewEnabled)
            {
                stopThisPreview();
            }
        }
    }

    public bool OverCollider { get; set; }

    //Unity Monobehaviour Methods
    void Awake()
    {
        ThisPreviewEnabled = activateInAwake;
    }

    //Called when mouse enters object collider
    IEnumerator OnMouseEnter()
    {
        if (secondsTillPreview == 0)
        {
            OverCollider = true;
            if (PreviewsAllowed && ThisPreviewEnabled)
            {
                previewThisObject();
            }
        }
        else
        {
            yield return new WaitForSeconds(secondsTillPreview);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit) && hit.collider == gameObject.GetComponent<BoxCollider>())
            {
                OverCollider = true;
                if (PreviewsAllowed && ThisPreviewEnabled)
                {
                    previewThisObject();
                }
            }
        }
    }

    void OnMouseExit()
    {
        OverCollider = false;
        if (!previewingSomeCard())
        {
            stopAllPreviews();
        }
    }


    //Our own methods
    void previewThisObject()
    {
        //Disable previous preview if there is one
        stopAllPreviews();
        //Save this HoverPreview as current
        currentlyViewing = this;
        //Enable preview game object
        previewGameObject.SetActive(true);
        //Disable game object if we have it to disable (disable card in hand)
        if(turnOffWhenPreviewing != null)
        {
            turnOffWhenPreviewing.SetActive(false);
        }
        //Move and tween preview to position
        previewGameObject.transform.localPosition = Vector3.zero;
        previewGameObject.transform.localScale = Vector3.one;
        previewGameObject.transform.eulerAngles = Vector3.zero;
        previewGameObject.transform.DOLocalMove(targetPosition, 1f).SetEase(Ease.OutQuint);
        previewGameObject.transform.DOScale(targetScale, 1f).SetEase(Ease.OutQuint);
    }
    void stopThisPreview()
    {
        previewGameObject.SetActive(false);
        previewGameObject.transform.localPosition = Vector3.zero;
        previewGameObject.transform.localScale = Vector3.one;
        if(turnOffWhenPreviewing != null)
        {
            turnOffWhenPreviewing.SetActive(true);
        }
    }

    //Static methods
    private static void stopAllPreviews()
    {
        if(currentlyViewing != null)
        {
            currentlyViewing.stopThisPreview();
        }
    }

    private static bool previewingSomeCard()
    {
        if (!previewsAllowed)
        {
            return false;
        }
        HoverPreview[] allHoverPreviews = GameObject.FindObjectsOfType<HoverPreview>();

        foreach (HoverPreview hp in allHoverPreviews)
        {
            if(hp.OverCollider && hp.thisPreviewEnabled)
            {
                return true;
            }
        }

        return false;
    }
}
