﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ManaCountVisual : MonoBehaviour
{
    public int testFullCrystals;
    public int testTotalCrystalsThisTurn;

    public Image[] crystals;
    public Text progressText;

    private int totalCrystals;
    public int TotalCrystals
    {
        get
        {
            return totalCrystals;
        }
        set
        {
            if(value > crystals.Length)
            {
                totalCrystals = crystals.Length;
            }
            else if(value < 0)
            {
                totalCrystals = 0;
            }
            else
            {
                totalCrystals = value;
            }

            for (int i = 0; i < crystals.Length; i++)
            {
                if (i < totalCrystals)
                {
                    if (crystals[i].color == Color.clear)
                    {
                        crystals[i].color = Color.gray;
                    }
                }
                else
                {
                    crystals[i].color = Color.clear;
                }
            }

            progressText.text = string.Format("{0}/{1}", AvailableCrystals.ToString(), TotalCrystals.ToString());
        }
    }

    private int availableCrystals;
    public int AvailableCrystals
    {
        get
        {
            return availableCrystals;
        }
        set
        {
            if(value > totalCrystals)
            {
                availableCrystals = totalCrystals;
            }
            else if(value < 0)
            {
                availableCrystals = 0;
            }
            else
            {
                availableCrystals = value;
            }

            for(int i = 0; i < totalCrystals; i++)
            {
                if(i < availableCrystals)
                {
                    crystals[i].color = Color.white;
                }
                else
                {
                    crystals[i].color = Color.gray;
                }
            }
            progressText.text = string.Format("{0}/{1}", availableCrystals.ToString(), totalCrystals.ToString());
        }
    }

    void Update()
    {
        if(Application.isEditor && !Application.isPlaying)
        {
            TotalCrystals = testTotalCrystalsThisTurn;
            AvailableCrystals = testFullCrystals;
        }
    }
}
