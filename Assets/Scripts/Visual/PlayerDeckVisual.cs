﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

//Attached to deck to generate cards to place in hand
public class PlayerDeckVisual : MonoBehaviour
{
    public AreaPosition owner;
    public GameObject DeckCube;
    public Canvas DeckCanvas;
    private float HightOfCard; //magic number. Maybe fix later

    void Start()
    {
        CardsInDeck = GlobalSettings.Instance.players[owner].deck.cards.Count;
        if(owner == AreaPosition.Bottom)
        {
            HightOfCard = (3.0f - 2.5f) / CardsInDeck;
            transform.position = new Vector3(transform.position.x, transform.position.y, 2.5f + (0.5f - (cardsInDeck - 1) * HightOfCard));
        }
        else
        {
            HightOfCard = (3.19f - 2.69f) / CardsInDeck;
            transform.position = new Vector3(transform.position.x, transform.position.y, 2.69f + (0.5f - (cardsInDeck - 1) * HightOfCard));
        }
    }

    private int cardsInDeck = 0;
    public int CardsInDeck
    {
        get
        {
            return cardsInDeck;
        }

        set
        {
            cardsInDeck = value;
            if(cardsInDeck == 0)
            {
                DeckCube.SetActive(false);
                DeckCanvas.enabled = false;
            }
            else
            {
                DeckCube.SetActive(true);
                DeckCanvas.enabled = true;
                if (owner == AreaPosition.Bottom)
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y, 2.5f + (0.5f - (cardsInDeck - 1) * HightOfCard));
                }
                else
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y, 2.69f + (0.5f - (cardsInDeck - 1) * HightOfCard));
                }
            }
        }
    }
}
