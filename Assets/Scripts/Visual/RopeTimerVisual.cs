﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using DG.Tweening;

public class RopeTimerVisual : MonoBehaviour, IEventSystemHandler
{
    public GameObject ropeGameObject;
    public Slider ropeSlider;
    public Text timerText;

    public float ropeBurnTime;
    private float timeTillZero;

    private bool counting = false;
    private bool isRopeBurning;

    void Awake()
    {
        if (ropeGameObject != null)
        {
            ropeSlider.minValue = 0;
            ropeSlider.maxValue = ropeBurnTime;
            ropeGameObject.SetActive(false);
        }
    }

    public void StartTimer(float timeForOneTurn)
    { 
            timeTillZero = timeForOneTurn;
            counting = true;
            isRopeBurning = false;
            if (ropeGameObject != null)
            {
                ropeGameObject.SetActive(false);
            }
            Command.CommandExecutionComplete();
    }

    void Update()
    {
        if (counting)
        {
            timeTillZero = timeTillZero - Time.deltaTime;
            if (timerText != null)
            {
                timerText.text = ToString();
            }

            if (ropeGameObject != null)
            {
                if (timeTillZero <= ropeBurnTime && !isRopeBurning)
                {
                        isRopeBurning = true;
                        ropeGameObject.SetActive(true);
                }

                if (isRopeBurning)
                {
                    ropeSlider.value = timeTillZero;
                }
            }

            if (timeTillZero <= 0)
            {
                counting = false;
            }
        }
    }

    public void StopTimer()
    {
        counting = false;
        Command.CommandExecutionComplete();
        Sequence revert = DOTween.Sequence();
    }

    public override string ToString()
    {
        int inSeconds = Mathf.RoundToInt(timeTillZero);
        string justSeconds = (inSeconds % 60).ToString();
        if (justSeconds.Length == 1)
        {
            justSeconds = "0" + justSeconds;
        }
        string justMinutes = (inSeconds / 60).ToString();
        if (justMinutes.Length == 1)
        {
            justMinutes = "0" + justMinutes;
        }
        return string.Format("{0}:{1}", justMinutes, justSeconds);
    }
}