﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EqualDistanceObjects : MonoBehaviour
{
    //Used to make cards and servants (Objects) have equal rotation and position compared to each other

    public Transform[] objects;

    void Awake()
    {
        Vector3 firstObjectPos = objects[0].transform.position;
        Vector3 lastObjectPos = objects[objects.Length - 1].transform.position;

        float xDist = (lastObjectPos.x - firstObjectPos.x) / (float)(objects.Length - 1);
        float yDist = (lastObjectPos.y - firstObjectPos.y) / (float)(objects.Length - 1);
        float zDist = (lastObjectPos.z - firstObjectPos.z) / (float)(objects.Length - 1);

        Vector3 Dist = new Vector3(xDist, yDist, zDist);

        for (int i = 1; i < objects.Length; i++)
        {
            objects[i].transform.position = objects[i - 1].transform.position + Dist;
        }
    }
}
