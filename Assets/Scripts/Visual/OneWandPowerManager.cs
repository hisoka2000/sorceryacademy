﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//holds references to all the text and images on the card
public class OneWandPowerManager : MonoBehaviour
{
    public OneWandPowerManager previewManager;       //reference to this object
    [Header("Text References")]
    public Text nameText;                       //Text values for the cards name, mana etc.
    public Text manaText;
    public Text descriptionText;
    [Header("Image References")]
    public Image cardIllustrationImage;
    public Image cardFrameImage;
    public Image cardTypeImage; //Image of area of card where card type is shown and the frame of the card.
    public CardAsset cardAsset;

    public void ReadCardFromAsset()
    {
        cardFrameImage.sprite = cardAsset.characterAsset.ClassSpellFrameImage;
        cardTypeImage.sprite = cardAsset.characterAsset.ClassCardTypeImage;
        nameText.text = cardAsset.cardName;
        manaText.text = cardAsset.ManaCost.ToString();
        descriptionText.text = cardAsset.Description;
        cardIllustrationImage.sprite = cardAsset.cardIllustration;
    }
}
