﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HandVisual : MonoBehaviour
{
    public AreaPosition owner;
    public bool TakeCardsOpenly = true;
    public EqualCards slots;

    [Header("Transform References")]
    public Transform drawPreviewSpot;
    public Transform deckTransform;
    public Transform otherCardDrawSourceTransform;
    public Transform playPreviewSpot;

    [Header("PlayerIllustrationVisual Reference for Card Back")]
    public PlayerIllustrationVisual playerIllustrationVisual;

    //List of all card visual representations as GameObjects
    private List<GameObject> CardsInHand = new List<GameObject>();

    //Adding and removing cards from hand

    public void AddCard(GameObject card)
    {
        //Insert card at 0th element in CardsHand (Rightmost card)
        CardsInHand.Insert(0, card);

        //parent card to slots GameObject
        card.transform.SetParent(slots.transform);

        //re-position cards in hand
        PlaceCardsInNewSlots();
        UpdatePlacementOfSlots();
    }

    public void RemoveCard(GameObject card)
    {
        //Remove card from list
        CardsInHand.Remove(card);

        //re-position cards in hand
        PlaceCardsInNewSlots();
        UpdatePlacementOfSlots();
    }

    //Remove card with given index in hand
    public void RemoveCardAtIndex(int index)
    {
        CardsInHand.RemoveAt(index);

        //re=position cards in hand
        PlaceCardsInNewSlots();
        UpdatePlacementOfSlots();
    }

    public GameObject GetCardAtIndex(int index)
    {
        return CardsInHand[index];
    }

    //Managing cards and slots

    //Move and ROTATE Slots GameObject according to number of cards in hand
    void UpdatePlacementOfSlots()
    {
        //Initial rotation of slots gameobject (set to 20)
        Vector3 initialRot = new Vector3(0, 0, 20);
        //Calculating average zRotation
        float zRot = initialRot.z / (float)(slots.objects.Length - 1);
        //Rotation vector based on zRot
        Vector3 Rot = new Vector3(0, 0, zRot);

        float posX;
        if(CardsInHand.Count > 0)
        {
            posX = (slots.objects[0].transform.localPosition.x - slots.objects[CardsInHand.Count - 1].transform.localPosition.x) / 2f;
        }
        else
        {
            posX = 0f;
        }
        slots.gameObject.transform.DOLocalMoveX(posX, 0.3f);
        //TODO change if statement so that when player at top is AI the cards are rotated
        if (owner == AreaPosition.Bottom)
        {
            slots.gameObject.transform.DOLocalRotate(initialRot - (CardsInHand.Count - 1) * Rot, 0.3f);
        }
    }

    //Shift all cards to their new slots
    public void PlaceCardsInNewSlots()
    {
        foreach (GameObject g in CardsInHand)
        {
            g.transform.DOLocalMove(slots.objects[CardsInHand.IndexOf(g)].transform.localPosition, 0.3f); //BUGCHECK: removed localPosition.x
            //TODO change if statement so that when player at top is AI the cards are rotated
            if (owner == AreaPosition.Bottom)
            {
                g.transform.DOLocalRotate(slots.objects[CardsInHand.IndexOf(g)].transform.localRotation.eulerAngles, 0.3f);
            }


            // apply correct sorting order and HandSlot value for later 
            WhereIsTheCardOrServant w = g.GetComponent<WhereIsTheCardOrServant>();
            w.Slot = CardsInHand.IndexOf(g);
            w.SetHandSortingOrder();
        }
    }

    //Card draw

    // creates a card and returns a new card as a GameObject
    GameObject CreateACardAtPosition(CardAsset c, Vector3 position, Vector3 eulerAngles)
    {
        // Instantiate a card depending on its type
        GameObject card;
        if (c.MaxHealth > 0)
        {
            // this is a servant card
            if(c.targets == TargettingOption.NoTarget)
            {
                card = GameObject.Instantiate(GlobalSettings.Instance.ServantCardPrefab, position, Quaternion.Euler(eulerAngles)) as GameObject;
            }
            else
            {
                card = GameObject.Instantiate(GlobalSettings.Instance.TargettedServantCardPrefab, position, Quaternion.Euler(eulerAngles)) as GameObject;
                DragServantOnTarget dragServant = card.GetComponentInChildren<DragServantOnTarget>();
                dragServant.Targets = c.targets;
            }
        }
        else
        {
            // this is a spell
            if (c.targets == TargettingOption.NoTarget)
            {
                //non-targetted spell
                card = GameObject.Instantiate(GlobalSettings.Instance.NoTargetSpellCardPrefab, position, Quaternion.Euler(eulerAngles)) as GameObject;
            }
            else
            {
                card = GameObject.Instantiate(GlobalSettings.Instance.TargettedSpellCardPrefab, position, Quaternion.Euler(eulerAngles)) as GameObject;
                // pass targeting options to DraggingActions
                DragSpellOnTarget dragSpell = card.GetComponentInChildren<DragSpellOnTarget>();
                dragSpell.Targets = c.targets;
            }

        }

        // apply look of card based on CardAsset
        OneCardManager manager = card.GetComponent<OneCardManager>();
        manager.cardAsset = c;
        manager.ReadCardFromAsset();

        // apply card back from character asset
        manager.cardBackImage.sprite = playerIllustrationVisual.characterAsset.CardBackImage;

        return card;
    }


    // gives player a new card from a given position
    public void GivePlayerACard(CardAsset c, int UniqueID, bool fast = false, bool fromDeck = true)
    {
        GameObject card;
        if (fromDeck)
        {
            card = CreateACardAtPosition(c, deckTransform.position + new Vector3(0, 0, -1), new Vector3(0f, -179f, -90f));
        }
        else
        {
            card = CreateACardAtPosition(c, otherCardDrawSourceTransform.position, new Vector3(0f, -179f, 0f));
        }

        // Set a tag to reflect where this card is
        foreach (Transform t in card.GetComponentsInChildren<Transform>())
        {
            t.tag = owner.ToString() + "Card";
        }

        // pass this card to HandVisual class
        AddCard(card);

        // Bring card to front while it travels from draw spot to hand
        WhereIsTheCardOrServant w = card.GetComponent<WhereIsTheCardOrServant>();
        w.BringAboveEverything();
        w.Slot = 0;
        w.State = VisualStates.Transition;

        // pass a unique ID to this card.
        IDHolder id = card.AddComponent<IDHolder>();
        id.UniqueID = UniqueID;

        // move card to the hand;
        Sequence s = DOTween.Sequence();
        if (!fast)
        {
            s.Append(card.transform.DOMove(drawPreviewSpot.position, GlobalSettings.Instance.cardTransitionTime));
            if (TakeCardsOpenly)
            {
                s.Insert(0f, card.transform.DORotate(Vector3.zero, GlobalSettings.Instance.cardTransitionTime));
            }
            else
            {
                s.Insert(0f, card.transform.DORotate(new Vector3(0f, 179f, 0f), GlobalSettings.Instance.cardTransitionTime));
            }
            s.AppendInterval(GlobalSettings.Instance.cardPreviewTime);
            //SetHandSortingOrder here so that cards move to the hand from deck at already correct sorting order
            w.SetHandSortingOrder();
            // displace the card to select it in the scene easier.
            s.Append(card.transform.DOLocalMove(slots.objects[0].transform.localPosition, GlobalSettings.Instance.cardTransitionTime));
            //TODO change if statement so that when player at top is AI the cards are rotated
            if (owner == AreaPosition.Bottom)
            {
                s.Join(card.transform.DOLocalRotate(slots.objects[0].transform.localRotation.eulerAngles, GlobalSettings.Instance.cardTransitionTime));
            }
        }
        else
        {
            if (TakeCardsOpenly)
            {
                s.Insert(0f, card.transform.DORotate(Vector3.zero, GlobalSettings.Instance.cardTransitionTImeFast));
            }
            else
            {
                s.Insert(0f, card.transform.DORotate(new Vector3(0f, 179f, 0f), GlobalSettings.Instance.cardTransitionTImeFast));
            }
            //SetHandSortingOrder here so that cards move to the hand from deck at already correct sorting order
            w.SetHandSortingOrder();
            // displace the card to select it in the scene easier.
            s.Insert(0f, card.transform.DOLocalMove(slots.objects[0].transform.localPosition, GlobalSettings.Instance.cardTransitionTImeFast));
            //TODO change if statement so that when player at top is AI the cards are rotated
            if (owner == AreaPosition.Bottom)
            {
                s.Join(card.transform.DOLocalRotate(slots.objects[0].transform.localRotation.eulerAngles, GlobalSettings.Instance.cardTransitionTImeFast));
            }
        }

        s.OnComplete(() => ChangeLastCardStatusToInHand(card, w));
    }

    // this method will be called when the card arrived to hand 
    void ChangeLastCardStatusToInHand(GameObject card, WhereIsTheCardOrServant w)
    {
        if (owner == AreaPosition.Bottom)
        {
            w.State = VisualStates.BottomHand;
        }
        else
        {
            w.State = VisualStates.TopHand;
        }

        // set correct sorting order
        w.SetHandSortingOrder();
        // end command execution for DrawACardCommand
        Command.CommandExecutionComplete();
    }

    //playing spells

    public void PlayASpellFromHand(int CardID)
    {
        GameObject card = IDHolder.GetGameObjectWIthID(CardID);
        PlayASpellFromHand(card);
    }

    public void PlayASpellFromHand(GameObject CardVisual)
    {
        Command.CommandExecutionComplete();
        CardVisual.GetComponent<WhereIsTheCardOrServant>().State = VisualStates.Transition;
        RemoveCard(CardVisual);

        CardVisual.transform.SetParent(null);
        CardVisual.GetComponent<OneCardManager>().cardFaceGlowImageObject.SetActive(false);
        Sequence s = DOTween.Sequence();
        s.Append(CardVisual.transform.DOMove(playPreviewSpot.position, 1f));
        s.Insert(0f, CardVisual.transform.DORotate(Vector3.zero, 1f));
        s.AppendInterval(2f);
        s.OnComplete(() =>
        {
            Destroy(CardVisual);
        });
    }
}
