﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AreaPosition { Top, Bottom}
public class PlayerArea : MonoBehaviour
{
    public AreaPosition owner;
    public bool controlsON = true;
    public PlayerDeckVisual playerDeck;
    public ManaCountVisual manaCount;
    public HandVisual hand;
    public PlayerIllustrationVisual playerIllustration;
    public WandPowerButton wandPowerButton;
    //public EndTurnButton endTurnButton;
    public TableVisual tableVisual;
    public Transform illustrationPosition;
    public Transform startPlayerIllustrationSpot;

    public bool AllowedToControlThisPlayer
    {
        get;
        set;
    }
}
