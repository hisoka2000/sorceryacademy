﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtonsScript : MonoBehaviour
{
    public LevelChanger levelChanger;

    public void LoadGameWithAI()
    {
        levelChanger.FadeToLevel(2);
    }

    public void LoadGameWithPlayer()
    {
        levelChanger.FadeToLevel(1);
    }

    public void LoadGameAIvsAI()
    {
        levelChanger.FadeToLevel(3);
    }
}
