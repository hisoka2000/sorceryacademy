﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MenuOptions : MonoBehaviour
{
    public List<GameObject> Characters = new List<GameObject>();
    public GameObject gameData;
    public bool isPlayer;
    public GameObject buttons;

    public void SetCharacterOption(GameObject character)
    {
        foreach(GameObject g in Characters)
        {
            if(g != character)
            {
                g.GetComponent<MenuPlayerIllustration>().glowObject.SetActive(false);
            }
        }
        character.GetComponent<MenuPlayerIllustration>().glowObject.SetActive(true);
        if (isPlayer)
        {
            gameData.GetComponent<GameData>().playerAsset = character.GetComponent<MenuPlayerIllustration>().characterAsset;
            if(gameData.GetComponent<GameData>().enemyAsset != null)
            {
                buttons.SetActive(true);
                buttons.transform.DOScale(1f, 0.6f);
            }
        }
        else
        {
            gameData.GetComponent<GameData>().enemyAsset = character.GetComponent<MenuPlayerIllustration>().characterAsset;
            if (gameData.GetComponent<GameData>().playerAsset != null)
            {
                buttons.SetActive(true);
                buttons.transform.DOScale(1f, 0.6f);
            }
        }
    }
}
