﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;


public class MenuPlayerIllustration : MonoBehaviour
{
    public CharacterAsset characterAsset;
    [Header("Text References")]
    public Text healthText;
    [Header("Image References")]
    public Image personaIllustration;
    public GameObject glowObject;

    void Awake()
    {
        if (characterAsset != null)
        {
            ReadCharacterFromAsset();
        }
    }

    //Sets healthtext, wandpowerimage, illustration and deck illustration
    public void ReadCharacterFromAsset()
    {
        healthText.text = characterAsset.MaxHealth.ToString();
        personaIllustration.sprite = characterAsset.AvatarImage;
    }

    public void OnMouseDown()
    {
        GetComponentInParent<MenuOptions>().SetCharacterOption(gameObject);
    }
}
