﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalSettings : MonoBehaviour
{
    [Header("Players")]
    public Player TopPlayer;
    public Player BottomPlayer;
    [Header("Number Values")]
    public float cardPreviewTime = 1f;
    public float cardTransitionTime = 1f;
    public float cardPreviewTimeFast = 0.2f;
    public float cardTransitionTImeFast = 0.5f;
    [Header("Prefabs")]
    public GameObject NoTargetSpellCardPrefab;
    public GameObject TargettedSpellCardPrefab;
    public GameObject ServantCardPrefab;
    public GameObject TargettedServantCardPrefab;
    public GameObject ServantPrefab;
    public GameObject DamageEffectPrefab;
    public GameObject ExplosionPrefab;
    [Header("Other")]
    public GameObject ropeGameObjectInGame;
    public Button EndTurnButton;
    public GameObject GameOverCanvas;

    public Dictionary<AreaPosition, Player> players = new Dictionary<AreaPosition, Player>();

    public static GlobalSettings Instance;

    void Awake()
    {
        players.Add(AreaPosition.Top, TopPlayer);
        players.Add(AreaPosition.Bottom, BottomPlayer);
        Instance = this;
}

public bool CanControlThisPlayer(AreaPosition owner)
    {
        bool playersTurn = (TurnManager.Instance.WhoseTurn == players[owner]);
        bool notDrawingCards = !Command.isCardDrawPending();
        return players[owner].playerArea.AllowedToControlThisPlayer && players[owner].playerArea.controlsON && playersTurn && notDrawingCards;
    }

    public bool CanControlThisPlayer(Player ownerPlayer)
    {
        bool playersTurn = (TurnManager.Instance.WhoseTurn == ownerPlayer);
        bool notDrawingCards = !Command.isCardDrawPending();
        return ownerPlayer.playerArea.AllowedToControlThisPlayer && ownerPlayer.playerArea.controlsON && playersTurn && notDrawingCards;
    }

    public void EnableEndTurnButtonOnStart(Player player)
    {
        if((player == BottomPlayer && CanControlThisPlayer(AreaPosition.Bottom)) || (player == TopPlayer && CanControlThisPlayer(AreaPosition.Top)))
        {
            EndTurnButton.interactable = true;
        }
        else
        {
            EndTurnButton.interactable = false;
        }
    }

    //TEST for COMMANDEXECUTION ERRORS

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            Command.CommandExecutionComplete();
        }
    }
}
