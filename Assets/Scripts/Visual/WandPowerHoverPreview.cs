﻿using System.Collections;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class WandPowerHoverPreview : MonoBehaviour
{
    //Position of preview relative to card ((0, 0, 0) for exact same position as card)
    public Vector3 targetPosition;
    //Scale of card preview relative to card (1 for exact same scale as card)
    public float targetScale;
    //Seconds to wait till the preview appears
    public float secondsTillPreview = 0;
    public GameObject previewGameObject;
    public GameObject ParentGameObject;


    public bool OverCollider { get; set; }

    //Called when mouse enters object collider
    IEnumerator OnMouseEnter()
    {
        if (secondsTillPreview == 0)
        {
            OverCollider = true;
            previewThisObject();
        }
        else
        {
            yield return new WaitForSeconds(secondsTillPreview);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit) && hit.collider == gameObject.GetComponent<MeshCollider>())
            {
                OverCollider = true;
                previewThisObject();
            }
        }
    }

    void OnMouseExit()
    {
        OverCollider = false;
        stopThisPreview();
    }


    //Our own methods
    void previewThisObject()
    {
        //Enable preview game object
        previewGameObject.SetActive(true);
        //Move and tween preview to position
        previewGameObject.transform.localPosition = Vector3.zero;
        previewGameObject.transform.localScale = Vector3.one;
        previewGameObject.transform.eulerAngles = Vector3.zero;
        if (ParentGameObject.name.Contains("BOTTOM"))
        {
            previewGameObject.transform.DOLocalMove(targetPosition, 1f).SetEase(Ease.OutQuint);
            previewGameObject.transform.DOScale(targetScale, 1f).SetEase(Ease.OutQuint);
        }
        else
        {
            previewGameObject.transform.DOLocalMove(-targetPosition, 1f).SetEase(Ease.OutQuint);
            previewGameObject.transform.DOScale(targetScale, 1f).SetEase(Ease.OutQuint);
        }
    }
    void stopThisPreview()
    {
        previewGameObject.SetActive(false);
        previewGameObject.transform.localPosition = Vector3.zero;
        previewGameObject.transform.localScale = Vector3.one;
    }
}
