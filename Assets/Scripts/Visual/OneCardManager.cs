﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//holds references to all the text and images on the card
public class OneCardManager : MonoBehaviour
{
    public CardAsset cardAsset;                 //our custom card asset reference
    public OneCardManager previewManager;       //reference to this object
    [Header("Text References")]
    public Text nameText;                       //Text values for the cards name, mana etc.
    public Text manaText;
    public Text descriptionText;
    public Text healthText;
    public Text attackText;
    [Header("Sprites")]
    public Sprite neutralServantImage;
    public Sprite neutralTypeImage;
    public Sprite neutralSpellImage;
    [Header("Image References")]
    public Image cardIllustrationImage;
    public GameObject cardFaceGlowImageObject;             //Glow images around card showing that the card is active/can be played
    public Image cardBackGlowImage;
    public Image cardFrameImage;
    public Image cardTypeImage; //Image of area of card where card type is shown and the frame of the card.
    public Image cardBackImage;

    void Awake()                                //Used to setup components of THIS object
    {
        if(cardAsset != null)
        {
            ReadCardFromAsset();
        }
    }

    private bool canBePlayed = false;           //Check if the card can be played
    public bool CanBePlayed
    {
        get
        {
            return canBePlayed;
        }

        set
        {
            canBePlayed = value;
            cardFaceGlowImageObject.SetActive(value);  //If can be played, enable the glow image around card
            previewManager.cardFaceGlowImageObject.SetActive(value);
        }
    }

    public void ReadCardFromAsset()
    {
        if(cardAsset.characterAsset == null && cardAsset.MaxHealth == 0)
        {
            //coin card or some other neutral spell
            cardFrameImage.sprite = neutralSpellImage;
            cardTypeImage.sprite = neutralTypeImage;
        }
        //actions performed for all cards
        else if(cardAsset.characterAsset != null)
        {
            if(cardAsset.MaxHealth == 0)
            {
                cardFrameImage.sprite = cardAsset.characterAsset.ClassSpellFrameImage;
            }
            else
            {
                cardFrameImage.sprite = cardAsset.characterAsset.ClassServantFrameImage;
            }
            cardTypeImage.sprite = cardAsset.characterAsset.ClassCardTypeImage;
        }
        else
        {
            cardFrameImage.sprite = neutralServantImage;
            cardTypeImage.sprite = neutralTypeImage;
        }

        nameText.text = cardAsset.cardName;
        manaText.text = cardAsset.ManaCost.ToString();
        descriptionText.text = cardAsset.Description;
        //If the description of card has : it means we need to make everything before : bold as its a keyword
        if (descriptionText.text.Contains(":"))
        {
            string result = "<b>";
            int index = descriptionText.text.IndexOf(":");
            for(int i = 0; i < index; i++)
            {
                result = result + descriptionText.text[i];
            }
            result = result + "</b>";
            for(int i = index; i < descriptionText.text.Length; i++)
            {
                result = result + descriptionText.text[i];
            }
            descriptionText.text = result;
        }
        cardIllustrationImage.sprite = cardAsset.cardIllustration;

        if(cardAsset.MaxHealth != 0)    //if this is a servant
        {
            attackText.text = cardAsset.Attack.ToString();
            healthText.text = cardAsset.MaxHealth.ToString();
        }

        if(previewManager != null)  //If this is a card and not a preview
        {
            previewManager.cardAsset = cardAsset;
            previewManager.ReadCardFromAsset();
        }
    }
}
