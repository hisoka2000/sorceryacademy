﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DamageImpact : MonoBehaviour
{
    public GameObject Text;
    public GameObject Blood;

    public Text damageAmmount;
    public CanvasGroup thisCanvasGroup;
    public Image impactImage;
    public Vector3 finalScale;

    private IEnumerator ShowDamageImpact()
    {
        gameObject.transform.DOScale(finalScale, 0.5f).SetEase(Ease.OutBounce);
        yield return new WaitForSeconds(1f);
        Text.GetComponent<CircleOutline>().enabled = false;
        Blood.GetComponent<CircleOutline>().enabled = false;
        thisCanvasGroup.DOFade(0, 2f);
        yield return new WaitForSeconds(2f);
        Destroy(this.gameObject);
    }

    //Overloaded DamageImpact which tracks the parent gameobject (Servant) and moved damage impact accordingly.
    public static void CreateDamageImpact(Vector3 position, int damage, GameObject parent)
    {
        if(damage == 0)
        {
            return;
        }
        GameObject newDamageImpact = new GameObject();
        newDamageImpact = GameObject.Instantiate(DamageImpactTest.Instance.damageImpactPrefab, position, Quaternion.identity) as GameObject;
        newDamageImpact.SetActive(true);
        Vector3 originalScale = new Vector3(1.78f, 1.78f, 1.78f); //TODO: Hardcoded value, change later. Damage impact scale changes when parenting to servant
        newDamageImpact.transform.SetParent(parent.transform);
        newDamageImpact.GetComponent<DamageImpact>().finalScale = originalScale;
        DamageImpact di = newDamageImpact.GetComponent<DamageImpact>();
        if(damage < 0)
        {
            //Negative damage = healing
            di.damageAmmount.text = "+" + (-damage).ToString();
            di.impactImage.color = new Color(81f/255f, 207f/255f, 98f/255f); //Sets color of damageImpact to green
        }
        else
        {
            di.damageAmmount.text = "-" + damage.ToString();
        }
        di.StartCoroutine(di.ShowDamageImpact());
    }

    public static void CreateDamageImpact(Vector3 position, int damage)
    {
        if (damage == 0)
        {
            return;
        }
        GameObject newDamageImpact = GameObject.Instantiate(GlobalSettings.Instance.DamageEffectPrefab, position, Quaternion.identity) as GameObject;
        newDamageImpact.SetActive(true);
        DamageImpact di = newDamageImpact.GetComponent<DamageImpact>();
        if (damage < 0)
        {
            //Negative damage = healing
            di.damageAmmount.text = "+" + (-damage).ToString();
            di.impactImage.color = new Color(81f/255f, 207f/255f, 98f/255f); //Sets color of damageImpact to green
        }
        else
        {
            di.damageAmmount.text = "-" + damage.ToString();
        }
        di.StartCoroutine(di.ShowDamageImpact());
    }
}
