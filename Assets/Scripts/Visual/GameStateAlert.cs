﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

public class GameStateAlert : MonoBehaviour
{
    public Text AlertText;
    public GameObject AlertCanvas;
    public static GameStateAlert Instance;

    void Awake()
    {
        Instance = this;
        AlertCanvas.SetActive(false);
    }

    public void ShowAlert(string alertText, float duration)
    {
        StartCoroutine(ShowAlertCoroutine(alertText, duration));
    }

    IEnumerator ShowAlertCoroutine(string alertText, float duration)
    {
        Vector3 initialPos = gameObject.transform.position;
        AlertText.text = alertText;

        AlertCanvas.SetActive(true);
        gameObject.transform.DOScale(new Vector3(1, 1, 1), 1f).SetEase(Ease.OutBack);
        yield return new WaitForSeconds(duration);
        gameObject.transform.DOScale(initialPos, 1f).SetEase(Ease.InBack);
        yield return new WaitForSeconds(1f);
        AlertCanvas.SetActive(false);
        Command.CommandExecutionComplete();
    }

    //Testing with getkeydown
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            ShowAlert("YOUR TURN", 3f);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            ShowAlert("ENEMY TURN", 3f);
        }
    }
}
