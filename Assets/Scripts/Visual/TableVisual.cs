﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TableVisual : MonoBehaviour
{
    //Which character does table belong to (TOP OR BOTTOM)
    public AreaPosition owner;
    //Where we put new servants
    public EqualDistanceObjects slots;

    //List of all sevant cards on table
    private List<GameObject> ServantsOnTable = new List<GameObject>();
    //hovering over this tables colider with mouse?
    private bool cursorOverThisTable = false;
    //3d collider attached to this game object
    private BoxCollider col;

    //Hovering over any players table collider?
    public static bool CursorOverSomeTable
    {
        get
        {
            TableVisual[] bothTables = GameObject.FindObjectsOfType<TableVisual>();
            return (bothTables[0].CursorOverThisTable || bothTables[1].CursorOverThisTable);
        }
    }

    //Hovering over THIS players table collider
    public bool CursorOverThisTable
    {
        get
        {
            return cursorOverThisTable;
        }
    }

    void Awake()
    {
        col = GetComponent<BoxCollider>();
    }

    void Update()
    {
        //Cursor detection using raycast
        RaycastHit[] hits;
        //raycast to mousePosition and store all hits in array
        hits = Physics.RaycastAll(Camera.main.ScreenPointToRay(Input.mousePosition), 30f);

        bool passedThroughTableCollider = false;
        foreach(RaycastHit h in hits)
        {
            if(h.collider == col)
            {
                passedThroughTableCollider = true;
            }
        }
        cursorOverThisTable = passedThroughTableCollider;
    }

    //method to add servant to the table
    public void AddServantAtIndex(CardAsset ca, int uniqueID, int index)
    {
        //Relies on GlobalSettings
        GameObject servant = GameObject.Instantiate(GlobalSettings.Instance.ServantPrefab, slots.objects[index].transform.position, Quaternion.identity) as GameObject;
        //Tween sequence for appearing animation for servant
        Sequence servantAppear = DOTween.Sequence();
        servantAppear.Append(servant.transform.DOLocalMoveZ(-1, 0.5f));
        servantAppear.Insert(0, servant.transform.DOLocalRotate(new Vector3(-10, 0, 0), 0.5f));
        servantAppear.Append(servant.transform.DOLocalMoveZ(0, 0.5f));
        servantAppear.Insert(1, servant.transform.DOLocalRotate(new Vector3(0, 0, 0), 0.5f));

        //apply look from cardAsset
        OneServantManager manager = servant.GetComponent<OneServantManager>();
        manager.cardAsset = ca;
        manager.ReadServantFromAsset();

        //Add tag to servant according to owner
        foreach(Transform t in servant.GetComponentsInChildren<Transform>())
        {
            t.tag = owner.ToString() + "Servant";
        }

        //Parent a new servant gameObject to table slots
        servant.transform.SetParent(slots.transform);

        //Add new servant to list
        ServantsOnTable.Insert(index, servant);

        //Let this servant know about its position
        WhereIsTheCardOrServant w = servant.GetComponent<WhereIsTheCardOrServant>();
        w.Slot = index;
        if(owner == AreaPosition.Bottom)
        {
            w.State = VisualStates.BottomTable;
        }
        else
        {
            w.State = VisualStates.TopTable;
        }

        //Add uniqueID to this servant
        IDHolder id = servant.AddComponent<IDHolder>();
        id.UniqueID = uniqueID;

        // After the new servant is added update placement of all the other servants
        ShiftSlotsGameObject();
        PlaceServantsInNewSlots();
        Command.CommandExecutionComplete();
    }

    //in onDraggingInUpdate in DragServantOnTable we call this method to move the servants about the dragged card
    public void MoveServantsAboutIndex(int index)
    {
        foreach (GameObject g in ServantsOnTable)
        {
            float distance;
            if (ServantsOnTable.IndexOf(g) == 6)
            {
                distance = slots.objects[ServantsOnTable.IndexOf(g) - 1].transform.position.x - slots.objects[ServantsOnTable.IndexOf(g)].transform.position.x;
            }
            else
            {
                distance = slots.objects[ServantsOnTable.IndexOf(g)].transform.position.x - slots.objects[ServantsOnTable.IndexOf(g) + 1].transform.position.x;
            }
            if (ServantsOnTable.IndexOf(g) < index)
            {

                g.transform.DOLocalMoveX(slots.objects[ServantsOnTable.IndexOf(g)].transform.localPosition.x + distance, 0.3f);
            }

            if (ServantsOnTable.IndexOf(g) >= index)
                g.transform.DOLocalMoveX(slots.objects[ServantsOnTable.IndexOf(g)].transform.localPosition.x - distance, 0.3f);

        }
    }

    //returns an index for a new servant based on mouse position. For placeing new servants wherever we want on the table
    public int TablePosForNewServant(float mouseX)
    {
        // If there are no servants or if we are pointing to the right of all servants
        // Place to the right - table slots are flipped so 0 is on the right side
        if (ServantsOnTable.Count == 0 || mouseX > slots.objects[0].transform.position.x)
        {
            return 0;
        } //else if the cursor is to the left of all servants
        else if(mouseX < slots.objects[ServantsOnTable.Count - 1].transform.position.x)
        {
            return ServantsOnTable.Count;
        }
        for(int i = 0; i < ServantsOnTable.Count; i++)
        {
            if(mouseX < slots.objects[i].transform.position.x && mouseX > slots.objects[i + 1].transform.position.x)
            {
                return i + 1;
            }
        }
        Debug.Log("For some reason mouse position x is neither to the right, left or middle of our cards. Need to fix something");
        return 0;
    }

    //Destroy a servant
    public void RemoveServantWithID(int IDToRemove)
    {
        //TODO add servant death animation tween
        GameObject servantToRemove = IDHolder.GetGameObjectWIthID(IDToRemove);
        ServantsOnTable.Remove(servantToRemove);
        Destroy(servantToRemove);

        ShiftSlotsGameObject();
        PlaceServantsInNewSlots();
        Command.CommandExecutionComplete();
    }

    //Shifts the slots GameObject according to number of servants
    void ShiftSlotsGameObject()
    {
        float posX;
        if(ServantsOnTable.Count > 0)
        {
            posX = (slots.objects[0].transform.position.x - slots.objects[ServantsOnTable.Count - 1].transform.position.x) / 2f;
        }
        else
        {
            posX = 0;
        }
        slots.gameObject.transform.DOLocalMoveX(posX, 0.3f);
    }

    //Shift all servants and place them in new slots once a new servant is added or an old one dies
    public void PlaceServantsInNewSlots()
    {
        foreach (GameObject g in ServantsOnTable)
        {
            g.transform.DOLocalMoveX(slots.objects[ServantsOnTable.IndexOf(g)].transform.localPosition.x, 0.3f);
        }
    }
}
