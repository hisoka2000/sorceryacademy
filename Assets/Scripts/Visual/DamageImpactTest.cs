﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageImpactTest : MonoBehaviour
{
    //Going to delete this later, just for quick testing of the damage impact effect

    public GameObject damageImpactPrefab;
    public static DamageImpactTest Instance;

    void Awake()
    {
        Instance = this;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            DamageImpact.CreateDamageImpact(transform.position, Random.Range(1, 10));
        }
    }
}
