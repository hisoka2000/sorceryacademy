﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WandPowerButton : MonoBehaviour
{
    public AreaPosition owner;

    public GameObject Front;
    public GameObject Back;
    public GameObject Glow;

    public bool interactable = true;

    private bool wasUsed = false;
    public bool WasUsed
    {
        get
        {
            return wasUsed;
        }
        set
        {
            wasUsed = value;
            if (!wasUsed)
            {
                Front.SetActive(true);
                Back.SetActive(false);
            }
            else
            {
                Front.SetActive(false);
                Back.SetActive(true);
                Highlighted = false;
            }
        }
    }

    private bool highlighted = false;
    public bool Highlighted
    {
        get
        {
            return highlighted;
        }
        set
        {
            highlighted = value;
            Glow.SetActive(highlighted);
        }
    }

    void OnMouseDown()
    {
        if (interactable)
        {
            if (!wasUsed && Highlighted)
            {
                GlobalSettings.Instance.players[owner].UseWandPower();
                WasUsed = !WasUsed;
            }
        }
    }
}
