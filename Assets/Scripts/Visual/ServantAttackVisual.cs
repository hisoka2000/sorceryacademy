﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ServantAttackVisual : MonoBehaviour
{
    private OneServantManager manager;
    private WhereIsTheCardOrServant w;

    void Awake()
    {
        manager = GetComponent<OneServantManager>();
        w = GetComponent<WhereIsTheCardOrServant>();
    }

    public void AttackTarget(int targetUniqueID, int damageTakenByTarget, int damageTakenByAttacker, int attackerHealthAfter, int targetHealthAfter, Player attacker)
    {
        manager.CanAttack = false;
        GameObject target = IDHolder.GetGameObjectWIthID(targetUniqueID);

        //Bring servant above everything in sorting
        w.BringAboveEverything();
        VisualStates tempState;
        if (attacker.ID == GlobalSettings.Instance.TopPlayer.ID)
        {
            tempState = VisualStates.TopTable;
        }
        else
        {
            tempState = VisualStates.BottomTable;
        }
        Debug.Log("Tempstate is " + tempState);
        w.State = VisualStates.Transition;

        Vector3 currentPosition = gameObject.transform.position;
        Tween t = transform.DOMove(target.transform.position, 0.25f).SetEase(Ease.InCubic).OnComplete(() =>
        {
            if (damageTakenByTarget > 0)
            {
                DamageImpact.CreateDamageImpact(target.transform.position, damageTakenByTarget);
            }
            if (damageTakenByAttacker > 0)
            {
                DamageImpact.CreateDamageImpact(transform.position, damageTakenByAttacker, gameObject);
            }

            if (targetUniqueID == GlobalSettings.Instance.BottomPlayer.PlayerID || targetUniqueID == GlobalSettings.Instance.TopPlayer.PlayerID)
            {
                //target is a character
                target.GetComponent<PlayerIllustrationVisual>().healthText.text = targetHealthAfter.ToString();
            }
            else
            {
                target.GetComponent<OneServantManager>().healthText.text = targetHealthAfter.ToString();
            }

            manager.healthText.text = attackerHealthAfter.ToString();
        });

        Tween m = transform.DOMove(currentPosition, 0.5f).SetEase(Ease.InCubic).OnComplete(() =>
        {
            w.SetTableSortingOrder();
            w.State = tempState;
        });

        Sequence s = DOTween.Sequence();
        s.Append(t);
        s.Append(m);
        s.AppendInterval(1f);
        s.OnComplete(Command.CommandExecutionComplete);
    }
}
