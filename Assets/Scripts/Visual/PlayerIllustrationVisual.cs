﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;


public class PlayerIllustrationVisual : MonoBehaviour
{
    public CharacterAsset characterAsset;
    [Header("Text References")]
    public Text healthText;
    [Header("Image References")]
    public Image wandPowerImage;
    public Image personaIllustration;
    public Image deckBackIllustration;
    [Header("Other")]
    public OneWandPowerManager wandPowerManager;

    void Awake()
    {
        if(characterAsset != null)
        {
            ReadCharacterFromAsset();
        }
    }

    //Sets healthtext, wandpowerimage, illustration and deck illustration
    public void ReadCharacterFromAsset()
    {
        healthText.text = characterAsset.MaxHealth.ToString();
        wandPowerImage.sprite = characterAsset.WandPowerIcon;

        wandPowerManager.cardAsset = characterAsset.WandPowerCardAsset;
        wandPowerManager.ReadCardFromAsset();

        personaIllustration.sprite = characterAsset.AvatarImage;
        deckBackIllustration.sprite = characterAsset.CardBackImage;
    }

    public void TakeDamage(int damage, int healthAfter)
    {
        //TEST Removed if(damage > 0) condition to allow for heals
        DamageImpact.CreateDamageImpact(transform.position, damage);
        //If we are healing, and we are overhealing, set the healthText to the character assets MaxHealth
        if(healthAfter > characterAsset.MaxHealth)
        {
            healthAfter = characterAsset.MaxHealth;
        }
        healthText.text = healthAfter.ToString();
    }

    public void Explode()
    {
        //TODO: Make explosion happen on playerillustration
        /*
        //TEST comment next line
        Instantiate(GlobalSettings.Instance.ExplosionPrefab, transform.position, Quaternion.identity);
        Sequence s = DOTween.Sequence();
        s.PrependInterval(2f);
        //TEST comment next line
        s.OnComplete(() => GlobalSettings.Instance.GameOverCanvas.SetActive(true));
        */

        //Go back to main menu
        GameObject levelLoader = GameObject.Find("LevelChangerCanvas");
        levelLoader.GetComponentInChildren<LevelChanger>().FadeToLevel(0);
    }
}
