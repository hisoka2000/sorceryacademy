﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

public class OneServantManager : MonoBehaviour
{
    public CardAsset cardAsset;
    public OneCardManager previewManager;
    [Header("Text references")]
    public Text healthText;
    public Text attackText;
    [Header("Image references")]
    public Image servantIllustration;
    public GameObject servantGlowImageObject;
    public GameObject servantFrozenImageObject;

    void Awake()
    {
        if (cardAsset != null)
        {
            ReadServantFromAsset();
        }
    }

    private bool canAttack;
    public bool CanAttack
    {
        get
        {
            return canAttack;
        }

        set
        {
            canAttack = value;

            servantGlowImageObject.SetActive(value);
        }
    }

    private bool frozen;
    public bool Frozen
    {
        get
        {
            return frozen;
        }
        set
        {
            frozen = value;
            if (frozen == true)
            {
                servantGlowImageObject.SetActive(false);
            }
            servantFrozenImageObject.SetActive(value);
        }
    }

    public void ReadServantFromAsset()
    {
        servantIllustration.sprite = cardAsset.cardIllustration;

        attackText.text = cardAsset.Attack.ToString();
        healthText.text = cardAsset.MaxHealth.ToString();

        if (previewManager != null)
        {
            previewManager.cardAsset = cardAsset;
            previewManager.ReadCardFromAsset();
        }
    }

    public void TakeDamage(int damage, int healthAfter)
    {
        DamageImpact.CreateDamageImpact(transform.position, damage);
        if (healthAfter > cardAsset.MaxHealth)
        {
            healthAfter = cardAsset.MaxHealth;
        }
        healthText.text = healthAfter.ToString();
    }

    public void UpdateAttack(int attack)
    {
        attackText.text = attack.ToString();

        Transform temp = attackText.transform;
        Sequence s = DOTween.Sequence();
        Tween attackBig = attackText.transform.DOScale(temp.localScale * 2, 0.5f);
        Tween attackSmall = attackText.transform.DOScale(temp.localScale, 0.5f);
        s.Append(attackBig);
        s.Append(attackSmall);
        Command.CommandExecutionComplete();
    }
    public void UpdateHealth(int health)
    {
        healthText.text = health.ToString();

        Transform temp = healthText.transform;
        Sequence s = DOTween.Sequence();
        Tween healthBig = healthText.transform.DOScale(temp.localScale * 2, 0.5f);
        Tween healthSmall = healthText.transform.DOScale(temp.localScale, 0.5f);
        s.Append(healthBig);
        s.Append(healthSmall);
        Command.CommandExecutionComplete();
    }
}
