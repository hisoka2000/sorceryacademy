﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HoverServant : MonoBehaviour
{
    //responsible for enabling white background outline image for servant when hovering over it

    public Image whiteHoverImage;
    void OnMouseEnter()
    {
        if (whiteHoverImage != null)
        {
            whiteHoverImage.gameObject.SetActive(true);
        }
    }

    void OnMouseExit()
    {
        if (whiteHoverImage != null)
        {
            whiteHoverImage.gameObject.SetActive(false);
        }
    }
}
