﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//SUMMARY: Shows the back and front of the card based on the cards relative vector rotation

[ExecuteInEditMode]     //Makes it so that this script is not only executed in play mode but also in the Unity editor
public class CardRotation : MonoBehaviour
{
    //Variables
    public RectTransform CardFront;     //Parent game object for the card face graphics
    public RectTransform CardBack;      //Parent game object for the card back graphics
    private bool isBack = false;        //True when front view of the card is seen by the camera


    // Update is called once per frame
    void Update()
    {
        Vector3 pos = Camera.main.transform.position;
        Vector3 dir = (this.transform.position - pos).normalized;
        //Debug.DrawLine(pos, pos + dir * 10, Color.red, Mathf.Infinity);
        isBack = Vector3.Dot(transform.forward, dir) < 0;

        if (isBack)
        {
            CardFront.gameObject.SetActive(false);
            CardBack.gameObject.SetActive(true);
        }
        else
        {
            CardFront.gameObject.SetActive(true);
            CardBack.gameObject.SetActive(false);


        }

    }
}
