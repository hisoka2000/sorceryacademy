﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum VisualStates
{
    Transition,
    BottomHand,
    TopHand,
    BottomTable,
    TopTable,
    Dragging
}
public class WhereIsTheCardOrServant : MonoBehaviour
{
    private HoverPreview hoverPreview;
    private Canvas canvas;
    private int aboveEverythingSortingOrder = 500;

    private int slot = -1;
    public int Slot
    {
        get
        {
            return slot;
        }
        set
        {
            slot = value;
        }
    }

    private VisualStates state;
    public VisualStates State
    {
        get
        {
            return state;
        }

        set
        {
            state = value;
            switch (state)
            {
                case VisualStates.BottomHand:
                    hoverPreview.ThisPreviewEnabled = true;
                    break;
                case VisualStates.BottomTable:
                case VisualStates.TopTable:
                    hoverPreview.ThisPreviewEnabled = true;
                    break;
                case VisualStates.Transition:
                    hoverPreview.ThisPreviewEnabled = false;
                    break;
                case VisualStates.Dragging:
                    hoverPreview.ThisPreviewEnabled = false;
                    break;
                case VisualStates.TopHand:
                    hoverPreview.ThisPreviewEnabled = false;
                    break;
            }
        }
    }

    void Awake()
    {
        hoverPreview = GetComponent<HoverPreview>();
        if(hoverPreview == null)
        {
            hoverPreview = GetComponentInChildren<HoverPreview>();
        }
        canvas = GetComponentInChildren<Canvas>();
    }

    public void BringAboveEverything()
    {
        canvas.sortingOrder = aboveEverythingSortingOrder;
        canvas.sortingLayerName = "AboveEverything";
    }

    public void SetHandSortingOrder()
    {
        if(slot != -1)
        {
            canvas.sortingOrder = HandSortingOrder(slot);
        }
        canvas.sortingLayerName = "Cards";
    }

    public void SetTableSortingOrder()
    {
        canvas.sortingOrder = 0;
        canvas.sortingLayerName = "Servants";
    }

    private int HandSortingOrder(int placeInHand)
    {
        return ((placeInHand + 1) * 10);
    }
}
