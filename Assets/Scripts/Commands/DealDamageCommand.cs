﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DealDamageCommand : Command
{
    private int targetID;
    private int damage;
    private int healthAfter;

    public DealDamageCommand(int targetID, int damage, int healthAfter)
    {
        this.targetID = targetID;
        this.damage = damage;
        this.healthAfter = healthAfter;
    }

    public override void StartCommandExecution()
    {
        GameObject target = IDHolder.GetGameObjectWIthID(targetID);
        if(targetID == GlobalSettings.Instance.BottomPlayer.PlayerID || targetID == GlobalSettings.Instance.TopPlayer.PlayerID)
        {
            //Target is a character
            target.GetComponent<PlayerIllustrationVisual>().TakeDamage(damage, healthAfter);
        }
        else
        {
            //Target is a servant
            target.GetComponent<OneServantManager>().TakeDamage(damage, healthAfter);
        }
        CommandExecutionComplete();
    }
}
