﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServantDieCommand : Command
{
    private Player p;
    private int DeadServantID;

    public ServantDieCommand(int servantID, Player p)
    {
        this.p = p;
        this.DeadServantID = servantID;
    }

    public override void StartCommandExecution()
    {
        p.playerArea.tableVisual.RemoveServantWithID(DeadServantID);
    }
}
