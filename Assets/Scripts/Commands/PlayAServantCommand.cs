﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAServantCommand : Command
{
    private CardLogic cardLogic;
    private int tablePosition;
    private Player player;
    private int servantID;

    public PlayAServantCommand(CardLogic cardLogic, int tablePosition, Player player, int servantID)
    {
        this.cardLogic = cardLogic;
        this.tablePosition = tablePosition;
        this.player = player;
        this.servantID = servantID;
    }

    public override void StartCommandExecution()
    {
        //Remove and destroy the servant in players hand
        HandVisual playerHand = player.playerArea.hand;
        GameObject card = IDHolder.GetGameObjectWIthID(cardLogic.UniqueCardID);
        playerHand.RemoveCard(card);
        GameObject.Destroy(card);
        //Enable hover previews
        HoverPreview.PreviewsAllowed = true;
        //Move servant to the spot on table
        player.playerArea.tableVisual.AddServantAtIndex(cardLogic.ca, servantID, tablePosition);
    }
}