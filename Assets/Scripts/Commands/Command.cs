﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Command
{
    public static Queue<Command> CommandQ = new Queue<Command>();
    public static bool playingQ = false;

    public virtual void AddToQ()
    {
        CommandQ.Enqueue(this);
        if (!playingQ)
        {
            PlayCommandFromQ();
        }
    }

    public virtual void StartCommandExecution()
    {
        //Use tweening sequence to call CommandExecutionComplete on OnComplete()
    }

    public static void CommandExecutionComplete()
    {
        if(CommandQ.Count > 0)
        {
            PlayCommandFromQ();
        }
        else
        {
            playingQ = false;
        }
        if(TurnManager.Instance.WhoseTurn != null)
        {
            TurnManager.Instance.WhoseTurn.HighlightPlayableCards();
        }
    }

    public static void PlayCommandFromQ()
    {
        playingQ = true;
        CommandQ.Dequeue().StartCommandExecution();
    }

    public static bool isCardDrawPending()
    {
        foreach(Command c in CommandQ)
        {
            if(c is DrawACardCommand)
            {
                return true;
            }
        }
        return false;
    }
}
