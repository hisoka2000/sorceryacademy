﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawACardCommand : Command
{
    private Player player;
    private int positionInHand;
    private CardLogic cardLogic;
    private bool fast;
    private bool fromDeck;

    public DrawACardCommand(Player player, int positionInHand, CardLogic cardLogic, bool fast, bool fromDeck)
    {
        this.player = player;
        this.positionInHand = positionInHand;
        this.cardLogic = cardLogic;
        this.fast = fast;
        this.fromDeck = fromDeck;
    }

    public override void StartCommandExecution()
    {
        if (fromDeck)
        {
            player.playerArea.playerDeck.CardsInDeck--;
        }
        //TODO: player.playerArea.hand.GivePlayerACard(cardLogic.ca, cardLogic.UniqueCardID, positionInHand, fast, fromDeck);
        player.playerArea.hand.GivePlayerACard(cardLogic.ca, cardLogic.UniqueCardID, fast, fromDeck);
    }
}
