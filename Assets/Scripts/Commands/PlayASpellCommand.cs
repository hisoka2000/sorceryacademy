﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayASpellCommand : Command
{
    private CardLogic cardLogic;
    private Player player;

    public PlayASpellCommand(CardLogic cardLogic, Player player)
    {
        this.cardLogic = cardLogic;
        this.player = player;
    }

    public override void StartCommandExecution()
    {
        player.playerArea.hand.PlayASpellFromHand(cardLogic.UniqueCardID);
    }
}
