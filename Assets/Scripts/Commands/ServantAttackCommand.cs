﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServantAttackCommand : Command
{
    //Position of servant on enemy area that will be attacked
    //if Enemyindex is -1 then attack the enemy character
    private int TargetID;
    private int AttackerID;
    private int AttackerHealthAfter;
    private int TargetHealthAfter;
    private int DamageTakenByAttacker;
    private int DamageTakenByTarget;
    private Player attacker;

    public ServantAttackCommand(int targetID, int attackerID, int attackerHealthAfter, int targetHealthAfter, int damageTakenByAttacker, int damageTakenByTarget, Player attacker)
    {
        this.TargetID = targetID;
        this.AttackerID = attackerID;
        this.AttackerHealthAfter = attackerHealthAfter;
        this.TargetHealthAfter = targetHealthAfter;
        this.DamageTakenByAttacker = damageTakenByAttacker;
        this.DamageTakenByTarget = damageTakenByTarget;
        this.attacker = attacker;
    }

    public override void StartCommandExecution()
    {
        GameObject Attacker = IDHolder.GetGameObjectWIthID(AttackerID);

        Attacker.GetComponent<ServantAttackVisual>().AttackTarget(TargetID, DamageTakenByTarget, DamageTakenByAttacker, AttackerHealthAfter, TargetHealthAfter, attacker);
    }
}
