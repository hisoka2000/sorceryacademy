﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverCommand : Command
{
    private Player looser;

    public GameOverCommand(Player looser)
    {
        this.looser = looser;
    }

    public override void StartCommandExecution()
    {
        looser.playerArea.playerIllustration.Explode();
    }
}
