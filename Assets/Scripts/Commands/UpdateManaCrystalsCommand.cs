﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateManaCrystalsCommand : Command
{
    private Player player;
    private int totalMana;
    private int availableMana;

    public UpdateManaCrystalsCommand(Player player, int totalMana, int availableMana)
    {
        this.player = player;
        this.totalMana = totalMana;
        this.availableMana = availableMana;
    }

    public override void StartCommandExecution()
    {
        player.playerArea.manaCount.TotalCrystals = totalMana;
        player.playerArea.manaCount.AvailableCrystals = availableMana;
        CommandExecutionComplete();
    }
}
