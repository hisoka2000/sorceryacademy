﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeTimerCommand : Command
{
    private float timeForOneTurn;
    private bool shouldStop = false;

    public RopeTimerCommand(float timeForOneTurn)
    {
        this.timeForOneTurn = timeForOneTurn;
    }

    public RopeTimerCommand(bool shouldStop = true)
    {
        this.shouldStop = shouldStop;
    }

    public override void StartCommandExecution()
    {
        if (shouldStop)
        {
            GlobalSettings.Instance.ropeGameObjectInGame.GetComponent<RopeTimerVisual>().StopTimer();
        }
        else
        {
            GlobalSettings.Instance.ropeGameObjectInGame.GetComponent<RopeTimerVisual>().StartTimer(timeForOneTurn);
        }
    }

}
