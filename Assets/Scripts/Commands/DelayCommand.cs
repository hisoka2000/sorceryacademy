﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DelayCommand : Command
{
    float timeToWait;

    public DelayCommand(float timeToWait)
    {
        this.timeToWait = timeToWait;
    }

    public override void StartCommandExecution()
    {
        Sequence sequence = DOTween.Sequence();
        sequence.PrependInterval(timeToWait);
        sequence.OnComplete(CommandExecutionComplete);
    }
}
