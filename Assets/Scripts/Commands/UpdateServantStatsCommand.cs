﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateServantStatsCommand : Command
{
    private int attack;
    private int health;
    private ServantLogic servant;

    public UpdateServantStatsCommand(ServantLogic servant, int attack = -1, int health = -1)
    {
        this.attack = attack;
        this.health = health;
        this.servant = servant;
    }

    public override void StartCommandExecution()
    {
        GameObject g = IDHolder.GetGameObjectWIthID(servant.ID);
        if (attack == -1)
        {
            g.GetComponent<OneServantManager>().UpdateHealth(health);
        }
        else if(health == -1)
        {
            g.GetComponent<OneServantManager>().UpdateAttack(attack);
        }
        else
        {
            Debug.LogError("Buffed attack both attack and health are -1");
        }
    }

}
