﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameData : MonoBehaviour
{
    public CharacterAsset playerAsset;
    public CharacterAsset enemyAsset;

    public List<CardAsset> SaintDeck;
    public List<CardAsset> NecromancerDeck;
    public List<CardAsset> PyromancerDeck;
    public List<CardAsset> OracleDeck;

    // Start is called before the first frame update
    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }
}
