﻿using UnityEngine;
using UnityEditor;

public class CardUnityIntegration
{
    [MenuItem("Assets/Create/Card")]
    
    public static void CreateScriptableObject()
    {
        ScriptableObjectUtility.CreateAsset<CardAsset>();
    }
}
