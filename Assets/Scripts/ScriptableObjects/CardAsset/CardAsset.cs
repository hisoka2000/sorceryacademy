﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TargettingOption
{
    NoTarget,
    AllServants,
    EnemyServants,
    YourServants,
    AllCharacters,
    EnemyCharacters,
    YourCharacters
}
public class CardAsset : ScriptableObject
{
    //This object will hold info about the general card
    [Header("General Info")]
    public CharacterAsset characterAsset;   //What class does this card belong to. If this is set to null then it's a neutral card
    public string cardName;
    [TextArea(2, 3)]
    public string Description;  //Card description
    public Sprite cardIllustration;
    public int ManaCost;

    [Header("Servant Info")]
    public int MaxHealth;       //If maxhealth is set to 0 then the card is a spell
    public int Attack;
    public int AttacksForOneTurn = 1;
    public bool Charm;
    public bool Berserk;
    public bool Venomous;
    public bool Vampire;
    public string ServantScriptName;
    public int SpecialServantAmmount;

    [Header("Spell Info")]
    public string SpellScriptName;
    public int SpecialSpellAmmount;

    [Header("Targetting (Spells and Servants)")]
    public TargettingOption targets;
    public bool isPositiveEffect;
}
