﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public enum CharClass {Pyromancer, Oracle, Necromancer, Saint}

public class CharacterAsset : ScriptableObject
{
    public CharClass Class;
    public string ClassName;
    public int MaxHealth;
    public string WandPowerName;
    public CardAsset WandPowerCardAsset;
    public Sprite ClassSpellFrameImage;
    public Sprite ClassServantFrameImage;
    public Sprite ClassCardTypeImage;  //Image of area where type of card is written
    public Sprite CardBackImage;
    public Sprite AvatarImage;
    public Sprite WandPowerIcon;
}
