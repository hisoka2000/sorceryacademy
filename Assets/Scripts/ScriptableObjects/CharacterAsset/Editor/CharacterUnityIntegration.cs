﻿using UnityEngine;
using UnityEditor;

public class CharacterUnityIntegration
{
    [MenuItem("Assets/Create/Character")]

    public static void CreateScriptableObject()
    {
        ScriptableObjectUtility.CreateAsset<CharacterAsset>();
    }
}
