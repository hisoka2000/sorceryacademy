﻿using System.Collections;
using UnityEngine;

public abstract class DraggingActions : MonoBehaviour
{
    public abstract void onStartDrag();
    public abstract void onEndDrag();
    public abstract void onDraggingInUpdate();

    public virtual bool CanDrag
    {
        get
        {
            //TEST
            //return true;
            return GlobalSettings.Instance.CanControlThisPlayer(playerOwner);
        }
    }

    protected virtual Player playerOwner
    {
        get
        {
            if (tag.Contains("Bottom"))
            {
                return GlobalSettings.Instance.BottomPlayer;
            }
            else if (tag.Contains("Top"))
            {
                return GlobalSettings.Instance.TopPlayer;
            }
            else
            {
                Debug.LogError("Player owner cannot be found as card or creature is untagged " + transform.parent.name);
                return null;
            }
        }
    }

    protected abstract bool DragSuccessful();
}
