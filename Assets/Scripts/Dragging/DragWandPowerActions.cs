﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DragWandPowerActions : DraggingActions
{
    public TargettingOption Options;

    public override void onStartDrag()
    {
        
    }

    public override void onDraggingInUpdate()
    {
        
    }

    public override void onEndDrag()
    {
        if (DragSuccessful())
        {

        }
        else
        {

        }
    }

    protected override bool DragSuccessful()
    {
        bool TableNotFull = (TurnManager.Instance.WhoseTurn.table.ServantsOnTable.Count < 8);

        return TableVisual.CursorOverSomeTable && TableNotFull;
    }
}
