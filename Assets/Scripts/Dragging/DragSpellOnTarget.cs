﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

public class DragSpellOnTarget : DraggingActions
{
    public TargettingOption Targets = TargettingOption.AllCharacters;
    private SpriteRenderer sr;
    private LineRenderer lr;
    private WhereIsTheCardOrServant whereIsThisCard;
    private VisualStates tempVisualState;
    private Transform arrow;
    private SpriteRenderer arrowSR;
    private GameObject Target;
    private OneCardManager manager;

    public override bool CanDrag
    {
        get
        {
            return base.CanDrag && manager.CanBePlayed;
        }
    }

    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        lr = GetComponentInChildren<LineRenderer>();
        lr.sortingLayerName = "AboveEverything";
        arrow = transform.Find("Arrow");
        arrowSR = arrow.GetComponent<SpriteRenderer>();

        manager = GetComponentInParent<OneCardManager>();
        whereIsThisCard = GetComponentInParent<WhereIsTheCardOrServant>();
    }


    public override void onStartDrag()
    {
        Cursor.visible = false;
        tempVisualState = VisualStates.BottomHand; //TODO: Check if works
        whereIsThisCard.State = VisualStates.Dragging;
        sr.enabled = true;
        lr.enabled = true;
    }

    public override void onDraggingInUpdate()
    {
        //Draws the arrow
        Vector3 direction = transform.position - transform.parent.position;
        Vector3 directionNormalized = direction.normalized;
        float distanceToTarget = (directionNormalized * 2.3f).magnitude;    //2.3f, magic number that works as distance to start showing arrow. Will change later

        if (direction.magnitude > distanceToTarget)
        {
            //Draw a line between servant and target
            lr.SetPositions(new Vector3[] { transform.parent.position, transform.position - directionNormalized * 2.3f });
            lr.enabled = true;

            //position end of arrow near target
            arrowSR.enabled = true;
            arrowSR.transform.position = transform.position - 1.5f * directionNormalized; //1.5f again a magic number, how close is arrow to target. Will change later

            //rotation of arrow
            float rot_z = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            arrowSR.transform.rotation = Quaternion.Euler(0f, 0f, rot_z);
        }
        else
        {
            //if target is not far enough, do not show the arrow
            lr.enabled = false;
            arrowSR.enabled = false;
        }
    }

    public override void onEndDrag()
    {
        Cursor.visible = true;

        Target = null;
        RaycastHit[] hits;
        hits = Physics.RaycastAll(origin: Camera.main.transform.position,
            direction: (this.transform.position - Camera.main.transform.position).normalized,
            maxDistance: 30f);

        foreach(RaycastHit h in hits)
        {
            if (h.transform.tag.Contains("Player"))
            {
                //Selected a player
                Target = h.transform.gameObject;
            }
            else if (h.transform.tag.Contains("Servant"))
            {
                Target = h.transform.parent.gameObject;
            }
        }

        bool targetValid = false;

        if(Target != null)
        {
            //Determine the owner of this card
            Player owner = null;
            if (tag.Contains("Bottom"))
            {
                owner = GlobalSettings.Instance.BottomPlayer;
            }
            else
            {
                owner = GlobalSettings.Instance.TopPlayer;
            }

            int targetID = Target.GetComponent<IDHolder>().UniqueID;
            switch (Targets)
            {
                case TargettingOption.AllCharacters:
                    owner.PlayASpellFromHand(GetComponentInParent<IDHolder>().UniqueID, targetID);
                    targetValid = true;
                    break;
                case TargettingOption.AllServants:
                    if (Target.tag.Contains("Servant"))
                    {
                        owner.PlayASpellFromHand(GetComponentInParent<IDHolder>().UniqueID, targetID);
                        targetValid = true;
                    }
                    break;
                case TargettingOption.EnemyCharacters:
                    if (Target.tag.Contains("Servant") || Target.tag.Contains("Player"))
                    {
                        // have to check that target is not a card
                        if ((tag.Contains("Bottom") && Target.tag.Contains("Top"))
                           || (tag.Contains("Top") && Target.tag.Contains("Bottom")))
                        {
                            owner.PlayASpellFromHand(GetComponentInParent<IDHolder>().UniqueID, targetID);
                            targetValid = true;
                        }
                    }
                    break;
                case TargettingOption.EnemyServants:
                    if (Target.tag.Contains("Servant"))
                    {
                        // have to check that target is not a card or a player
                        if ((tag.Contains("Bottom") && Target.tag.Contains("Top"))
                            || (tag.Contains("Top") && Target.tag.Contains("Bottom")))
                        {
                            owner.PlayASpellFromHand(GetComponentInParent<IDHolder>().UniqueID, targetID);
                            targetValid = true;
                        }
                    }
                    break;
                case TargettingOption.YourCharacters:
                    if (Target.tag.Contains("Servant") || Target.tag.Contains("Player"))
                    {
                        // have to check that target is not a card
                        if ((tag.Contains("Bottom") && Target.tag.Contains("Bottom"))
                            || (tag.Contains("Top") && Target.tag.Contains("Top")))
                        {
                            owner.PlayASpellFromHand(GetComponentInParent<IDHolder>().UniqueID, targetID);
                            targetValid = true;
                        }
                    }
                    break;
                case TargettingOption.YourServants:
                    if (Target.tag.Contains("Servant"))
                    {
                        // have to check that target is not a card or a player
                        if ((tag.Contains("Bottom") && Target.tag.Contains("Bottom"))
                            || (tag.Contains("Top") && Target.tag.Contains("Top")))
                        {
                            owner.PlayASpellFromHand(GetComponentInParent<IDHolder>().UniqueID, targetID);
                            targetValid = true;
                        }
                    }
                    break;
                default:
                    Debug.LogWarning("Reached default case in DragSpellOnTarget! Suspicious behaviour!!");
                    break;
            }
        }

        if (!targetValid)
        {
            whereIsThisCard.State = tempVisualState;
            whereIsThisCard.SetHandSortingOrder();
        }

        //Return target and arrow to original position
        //Using 0.4f z so that the arrow is on top of card
        transform.localPosition = new Vector3(0f, 0f, 0.4f);
        sr.enabled = false;
        lr.enabled = false;
        arrowSR.enabled = false;
    }

    // Not used in this script
    protected override bool DragSuccessful()
    {
        return true;
    }
}