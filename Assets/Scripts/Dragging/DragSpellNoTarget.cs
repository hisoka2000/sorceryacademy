﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DragSpellNoTarget : DraggingActions
{
    private int savedHandSlot;
    //Keep saved rotation so that on failed drag we tween back to it
    private WhereIsTheCardOrServant whereIsCard;
    private OneCardManager manager;

    public override bool CanDrag
    {
        get
        {
            return base.CanDrag && manager.CanBePlayed;
        }
    }

    void Awake()
    {
        whereIsCard = GetComponent<WhereIsTheCardOrServant>();
        manager = GetComponent<OneCardManager>();
    }

    public override void onStartDrag()
    {
        savedHandSlot = whereIsCard.Slot;
        whereIsCard.State = VisualStates.Dragging;
        whereIsCard.BringAboveEverything();
    }

    public override void onDraggingInUpdate()
    {
        
    }

    public override void onEndDrag()
    {
        if (DragSuccessful())
        {
            //if holding card over table, play the card
            playerOwner.PlayASpellFromHand(GetComponent<IDHolder>().UniqueID, -1);
        }
        else
        {
            //Set old sorting order
            whereIsCard.Slot = savedHandSlot;
            if (tag.Contains("Bottom"))
            {
                whereIsCard.State = VisualStates.BottomHand;
            }
            else
            {
                whereIsCard.State = VisualStates.TopHand;
            }
            whereIsCard.SetHandSortingOrder();
            //Move card back to its slot position
            HandVisual playerHand = playerOwner.playerArea.hand;
            Vector3 oldCardPosition = playerHand.slots.objects[savedHandSlot].transform.localPosition;
            transform.DOLocalMove(oldCardPosition, 0.6f);
        }
    }

    protected override bool DragSuccessful()
    {
        return TableVisual.CursorOverSomeTable;
    }
}
