﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

public class DragServantAttack : DraggingActions
{
    private SpriteRenderer sr;
    private LineRenderer lr;
    private WhereIsTheCardOrServant WhereIsThisServant;
    private Transform arrow;
    private SpriteRenderer arrowSR;
    private GameObject Target;
    private OneServantManager manager;

    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        lr = GetComponentInChildren<LineRenderer>();
        lr.sortingLayerName = "AboveEverything";
        arrow = transform.Find("Arrow");
        arrowSR = arrow.GetComponent<SpriteRenderer>();

        manager = GetComponentInParent<OneServantManager>();
        WhereIsThisServant = GetComponentInParent<WhereIsTheCardOrServant>();
    }

    public override bool CanDrag
    {
        get
        {
            //TEST line:
            //return true;

            return base.CanDrag && manager.CanAttack;
        }
    }

    public override void onStartDrag()
    {
        Cursor.visible = false;
        WhereIsThisServant.State = VisualStates.Dragging;
        sr.enabled = true;
        lr.enabled = true;
    }

    public override void onDraggingInUpdate()
    {
        //Draws the arrow
        Vector3 direction = transform.position - transform.parent.position;
        Vector3 directionNormalized = direction.normalized;
        float distanceToTarget = (directionNormalized * 2.3f).magnitude;    //2.3f, magic number that works as distance to start showing arrow. Will change later

        if (direction.magnitude > distanceToTarget)
        {
            //Draw a line between servant and target
            lr.SetPositions(new Vector3[] { transform.parent.position, transform.position - directionNormalized * 2.3f });
            lr.enabled = true;

            //position end of arrow near target
            arrowSR.enabled = true;
            arrowSR.transform.position = transform.position - 1.5f * directionNormalized; //1.5f again a magic number, how close is arrow to target. Will change later
            //rotation of arrow
            float rot_z = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            arrowSR.transform.rotation = Quaternion.Euler(0f, 0f, rot_z);
        }
        else
        {
            //if target is not far enough, do not show the arrow
            lr.enabled = false;
            arrowSR.enabled = false;
        }
    }

    public override void onEndDrag()
    {
        Cursor.visible = true;

        Target = null;
        RaycastHit[] hits;
        hits = Physics.RaycastAll(origin: Camera.main.transform.position,
            direction: (this.transform.position - Camera.main.transform.position).normalized,
            maxDistance: 30f);

        foreach (RaycastHit h in hits)
        {
            if ((h.transform.tag == "TopPlayer" && this.tag == "BottomServant") ||
                (h.transform.tag == "BottomPlayer" && this.tag == "TopServant"))
            {
                // go face
                Target = h.transform.gameObject;
            }
            else if ((h.transform.tag == "TopServant" && this.tag == "BottomServant") ||
                    (h.transform.tag == "BottomServant" && this.tag == "TopServant"))
            {
                // hit a servant, save parent transform
                Target = h.transform.parent.gameObject;
            }
        }

        bool targetValid = false;

        if (Target != null)
        {
            int targetID = Target.GetComponent<IDHolder>().UniqueID;
            Debug.Log("Target ID: " + targetID);
            if (targetID == GlobalSettings.Instance.BottomPlayer.PlayerID || targetID == GlobalSettings.Instance.TopPlayer.PlayerID)
            {
                // attack a character
                Debug.Log("Attacking " + Target);
                Debug.Log("TargetID: " + targetID);
                ServantLogic.ServantsCreatedThisGame[GetComponentInParent<IDHolder>().UniqueID].GoFace();
                targetValid = true;
            }
            else if (ServantLogic.ServantsCreatedThisGame[targetID] != null)
            {
                // if targeted servant is still alive, attack servant
                targetValid = true;
                ServantLogic.ServantsCreatedThisGame[GetComponentInParent<IDHolder>().UniqueID].AttackServantWithID(targetID);
                Debug.Log("Attacking " + Target);
            }
        }

        if (!targetValid)
        {
            //TODO: Can remove the if condition and use a tempState (see DragServantOnTable script)
            if (tag.Contains("Bottom"))
            {
                WhereIsThisServant.State = VisualStates.BottomTable;
            }
            else
            {
                WhereIsThisServant.State = VisualStates.TopTable;
            }
            WhereIsThisServant.SetTableSortingOrder();
        }

        //Return target and arrow to original position
        //Using 0.4f z so that the arrow is on top of card
        transform.localPosition = new Vector3(0f, 0f, 0.4f); //TODO: Change to Vector3.zero if necessary
        sr.enabled = false;
        lr.enabled = false;
        arrowSR.enabled = false;
    }

    // Not used in this script
    protected override bool DragSuccessful()
    {
        return true;
    }
}