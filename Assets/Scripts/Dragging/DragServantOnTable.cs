﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DragServantOnTable : DraggingActions
{
    private int savedHandSlot;
    private WhereIsTheCardOrServant whereIsCard;
    private IDHolder idScript;
    private VisualStates tempState;
    private OneCardManager manager;

    public override bool CanDrag
    {
        get
        {
            //TEST
            //return true;
            return base.CanDrag && manager.CanBePlayed;
        }
    }

    void Awake()
    {
        whereIsCard = GetComponent<WhereIsTheCardOrServant>();
        manager = GetComponent<OneCardManager>();
    }

    public override void onStartDrag()
    {
        savedHandSlot = whereIsCard.Slot;
        tempState = whereIsCard.State;
        whereIsCard.State = VisualStates.Dragging;
        whereIsCard.BringAboveEverything();
    }

    public override void onDraggingInUpdate()
    {
        int tablePos = playerOwner.playerArea.tableVisual.TablePosForNewServant(Camera.main.ScreenToWorldPoint(
                new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z - Camera.main.transform.position.z)).x);

        if (playerOwner.playerArea.tableVisual.CursorOverThisTable)
        {
            playerOwner.playerArea.tableVisual.MoveServantsAboutIndex(tablePos);
        }
        else
        {
            playerOwner.playerArea.tableVisual.PlaceServantsInNewSlots();
        }
    }

    public override void onEndDrag()
    {
        if (DragSuccessful())
        {
            //if we are holding card over table, determine table position for card
            int tablePos = playerOwner.playerArea.tableVisual.TablePosForNewServant(Camera.main.ScreenToWorldPoint(
                new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z - Camera.main.transform.position.z)).x);
            //play the card
            playerOwner.PlayAServantFromHand(GetComponent<IDHolder>().UniqueID, tablePos, -1);
        }
        else
        {
            //set old sorting order
            whereIsCard.SetHandSortingOrder();
            whereIsCard.State = tempState;
            //Move card back to its slot position
            HandVisual playerHand = playerOwner.playerArea.hand;
            Vector3 oldCardPosition = playerHand.slots.objects[savedHandSlot].transform.localPosition;
            transform.DOLocalMove(oldCardPosition, 0.6f);
        }
    }

    protected override bool DragSuccessful()
    {
        bool tableNotFull = (playerOwner.table.ServantsOnTable.Count < 8);
        return TableVisual.CursorOverSomeTable && tableNotFull;
    }
}
