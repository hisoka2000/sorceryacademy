﻿using System.Collections;
using UnityEngine;
using DG.Tweening;

public class Draggable : MonoBehaviour
{
    //Drag from the center of the card even when clicking on a non-center area of the card
    public bool usePointerDisplacement = true;

    public bool isServant = false;

    //Private Fields
    private DraggingActions da;

    private bool dragging = false;

    //Distance from the place where the card was clicked to the cards center
    private Vector3 pointerDistance = Vector3.zero;

    //Distance from camera to mouse on Z axis
    private float zDistance;

    //Check if dragging animation is already happening to stop clicks on animating objects
    private bool isAnimating = false;

    //Retain starting and current position of card for rotation calculation
    private Vector3 startingPosition;
    private Vector3 direction;

    //Rotation sensitivity and ammount when dragging
    private float rotationSensitivity = 0.5f;
    private float rotationAmmount = 15f;


    private static Draggable draggingThis;
    public static Draggable DraggingThis
    {
        get
        {
            return draggingThis;
        }
    }

    //Unity MonoBehaviour methods

    void Awake()
    {
        da = GetComponent<DraggingActions>();
    }
    void OnMouseDown()
    {
        if (da != null && da.CanDrag && !isAnimating)
        {
            startingPosition = transform.position;
            dragging = true;
            //When dragging should not allow previews
            HoverPreview.PreviewsAllowed = false;
            draggingThis = this;
            da.onStartDrag();
            zDistance = transform.position.z - Camera.main.transform.position.z;
            if (usePointerDisplacement)
            {
                pointerDistance = mouseInWorldCoords() - transform.position;
            }
            else
            {
                pointerDistance = Vector3.zero;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (dragging)
        {
            //Direction of card movement calculation for appropriate card rotation
            direction = movementDirection(startingPosition, transform.position);
            startingPosition = transform.position;

            //Moving and rotating card
            Vector3 mousePos = mouseInWorldCoords();
            transform.position = new Vector3(mousePos.x - pointerDistance.x, mousePos.y - pointerDistance.y, transform.position.z);
            da.onDraggingInUpdate();
            if (!isServant)
            {
                rotate(direction);
            }
        }
    }

    IEnumerator OnMouseUp()
    {
        if (dragging)
        {
            isAnimating = true;
            dragging = false;
            //Turn all previews back on
            HoverPreview.PreviewsAllowed = true;

            draggingThis = null;
            da.onEndDrag();
            yield return new WaitForSeconds(1f);
            isAnimating = false;
        }
    }

    //Helper methods

    //returns mouse position in World coordinates
    private Vector3 mouseInWorldCoords()
    {
        var screenMousePos = Input.mousePosition;
        screenMousePos.z = zDistance;
        return Camera.main.ScreenToWorldPoint(screenMousePos);
    }

    private Vector3 movementDirection(Vector3 startingPosition, Vector3 endingPosition)
    {
        Vector3 direction = endingPosition - startingPosition;
        direction = direction.normalized;
        return direction;
    }

    private void rotate(Vector3 direction)
    {
        if(direction.x > -rotationSensitivity && direction.x < rotationSensitivity)
        {
            if(direction.y > rotationSensitivity)
            {
                transform.DORotate(new Vector3(rotationAmmount, 0, 0), 1.5f).SetEase(Ease.OutQuint);
            }
            else if(direction.y < -rotationSensitivity)
            {
                transform.DORotate(new Vector3(-rotationAmmount, 0, 0), 1f).SetEase(Ease.OutQuint);
            }
            else
            {
                transform.DORotate(new Vector3(0, 0, 0), 2f).SetEase(Ease.OutQuint);
            }
        }

        if (direction.x >= rotationSensitivity)
        {
            if (direction.y > rotationSensitivity)
            {
                transform.DORotate(new Vector3(rotationAmmount, -rotationAmmount, 0), 1.5f).SetEase(Ease.OutQuint);
            }
            else if (direction.y < -rotationSensitivity)
            {
                transform.DORotate(new Vector3(-rotationAmmount, -rotationAmmount, 0), 1f).SetEase(Ease.OutQuint);
            }
            else
            {
                transform.DORotate(new Vector3(0, -rotationAmmount, 0), 2f).SetEase(Ease.OutQuint);
            }
        }

        if (direction.x <= -rotationSensitivity)
        {
            if (direction.y > rotationSensitivity)
            {
                transform.DORotate(new Vector3(rotationAmmount, rotationAmmount, 0), 1.5f).SetEase(Ease.OutQuint);
            }
            else if (direction.y < -rotationSensitivity)
            {
                transform.DORotate(new Vector3(-rotationAmmount, rotationAmmount, 0), 1f).SetEase(Ease.OutQuint);
            }
            else
            {
                transform.DORotate(new Vector3(0, rotationAmmount, 0), 2f).SetEase(Ease.OutQuint);
            }
        }
    }
}
