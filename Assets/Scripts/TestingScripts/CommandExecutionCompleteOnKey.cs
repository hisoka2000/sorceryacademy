﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandExecutionCompleteOnKey : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            Command.CommandExecutionComplete();
        }
    }
}
